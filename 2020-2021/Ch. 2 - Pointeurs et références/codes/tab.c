#include <stdio.h>

long unsigned sneaky(int array[])
{
	return sizeof(array) / sizeof(*array);
}

int main()
{
	int t1[5] = {1,2,3,4};
	int t2[] = {1,2,3,4,5};
	int * t3 = t2;
	//int t4[]; 
	//int t5[5] = {1,2,3,4,5,6}; 
	int t6[8];
	
	for(int i = 0; i < 5; i++)
		printf("%d %d %d %d\n", t1[i], t2[i], t3[i], t6[i]);
		
	printf("%lu\n", sizeof(t6) / sizeof(*t6));
	printf("%lu\n", sneaky(t6));
}
