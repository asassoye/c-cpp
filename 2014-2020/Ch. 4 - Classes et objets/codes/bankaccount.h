#ifndef ACC_H
#define ACC_H

#include <string>

class BankAccount
{
	double _balance;
	const std::string _name;

	public:
		BankAccount(std::string name, double balance = 0);	

		inline double balance() const;
		inline std::string name() const;

		void deposit(double amount);		
		void withdraw(double amount);

		void transfer(double amount, BankAccount& other);
}; //don't forget ;

double BankAccount::balance() const
{
	return _balance;
}

std::string BankAccount::name() const
{
	return _name;
}

#endif
