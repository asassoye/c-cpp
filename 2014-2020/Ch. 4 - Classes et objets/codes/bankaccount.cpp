#include "bankaccount.h"

//don't repeat = 0
BankAccount::BankAccount(std::string name, double balance) : _name(name), _balance(balance) 
{
	//_name = name;
	//_balance = balance;
}

void BankAccount::deposit(double amount)
{
	_balance += amount;
}

void BankAccount::withdraw(double amount)
{
	_balance -= amount;
}

void BankAccount::transfer(double amount, BankAccount& other)
{
	withdraw(amount);
	other.deposit(amount);
}
