#include <iostream>

using namespace std;

class A 
{
	int _x;

	public:
		A(int x) : _x(x) {} //try to comment that

		int x() { return _x; }
};

struct B
{
	A a;

	B() //: a(A(1))
	{}
};

int main()
{
	B b;
	//cout << b.a._x << endl;
	cout << b.a.x() << endl;
}
