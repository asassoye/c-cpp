#include <iostream>

#include "bankaccount.h"

using namespace std;

int main()
{
	BankAccount ba1("abs", 100);
	BankAccount ba2("nvs", 10);

	cout << "Balance " << ba1.name() << " : " << ba1.balance() << endl;
	cout << "Balance " << ba2.name() << " : " << ba2.balance() << endl;

	ba1.deposit(50);
	cout << "Balance " << ba1.name() << " : " << ba2.balance() << endl;

	ba1.transfer(20, ba2);
	cout << "Balance " << ba1.name() << " : " << ba1.balance() << endl;
	cout << "Balance " << ba2.name() << " : " << ba2.balance() << endl;
}
