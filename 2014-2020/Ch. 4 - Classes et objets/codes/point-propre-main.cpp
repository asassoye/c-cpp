#include <iostream>
#include "point-propre.h"

using namespace std;

int main()
{
	point p1;
	point p2(1,1);
	cout << p1.getX() << " " << p1.getY() << endl;
	sayHello(p1);
	sayHi(p1);
	cout << p2.getX() << " " << p2.getY() << endl;
	cout << "dist = " << p1.dist(p2) << endl;
	
	point p3(p1);//explicit copy
	p3 = p2;

	//try to remove default params and call default cstr
}
