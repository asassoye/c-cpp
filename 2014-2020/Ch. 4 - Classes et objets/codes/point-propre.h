#ifndef POINT_H
#define POINT_H

#include <cmath>

class point
{
	double x, y;
	bool copie;

	public:
		point(double x = 0, double y = 0);
		point(const point&);
		~point();
		inline double getX() const;
		inline double getY() const;
		inline double dist(point) const;
};

void sayHello(const point p);
void sayHi(const point& p);

double point::getX() const
{
	return x;
}

double point::getY() const
{
	return y;
}		

double point::dist(point p) const
{
	return sqrt((x - p.x)*(x - p.x)+(y - p.y)*(y - p.y));
}	

#endif
