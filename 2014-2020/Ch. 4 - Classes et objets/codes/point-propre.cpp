#include "point-propre.h"

#include <iostream>

point::point(double x, double y)
{
	this -> x = x;
	this -> y = y;
	this -> copie = false;

	std::cout << "Construction de " << x << " " << y << std::endl;
}

point::point(const point& p)
{
	this -> x = p.x;
	this -> y = p.y;
	this -> copie = true;

	std::cout << "Copie de " << x << " " << y << std::endl;
}

point::~point()
{
	std::cout << "Destruction de " << x << " " << y;
	if(copie)
		std::cout << " (copie)";
	std::cout << std::endl;
}

void sayHello(const point p)
{
	std::cout << "Hello Mr point " << p.getX() << " " << p.getY() << std::endl;
}

void sayHi(const point& p)
{
	std::cout << "Hi Mr point " << p.getX() << " " << p.getY() << std::endl;
}
