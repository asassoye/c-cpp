#include "sexy_point.h"

#include <cmath>

Point::Point(double x, double y) : x(x), y(y)
{
	//this->x = x;
	//this->y = y;
}

void Point::setX(double x)
{
	this-> x = x;
}

void Point::setY(double y)
{
	this-> y = y;
}

void Point::translate(double dx, double dy)
{
	x += dx;
	y += dy;
}

double Point::dist(const Point& pt) const
{
	return sqrt((x - pt.x) * (x - pt.x) + (y - pt.y) * (y - pt.y));
}
