class point
{
	double x, y;

	public:
		point(double x = 0, double y = 0);
		inline double getX() const;
		inline double getY() const;
		double dist(point p) const;
};

double point::getX() const
{
	return x;
}

double point::getY() const
{
	return y;
}
