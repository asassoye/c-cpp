#ifndef POINT_H
#define POINT_H

class Point
{
	double x, y; //coordinates

	public:
		Point(double x = 0, double y = 0);
		
		inline double getX() const;
		inline double getY() const;

		void setX(double x);
		void setY(double y);

		void translate(double dx, double dy);

		double dist(const Point& p) const;
};

double Point::getX() const
{
	return x;
}

double Point::getY() const
{
	return y;
}

#endif
