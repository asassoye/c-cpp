#include "point_decl.h"
#include <cmath>

point::point(double x, double y)
{
	this->x = x;
	this->y = y;
}

double point::dist(point p) const
{
	return sqrt((x - p.x)*(x - p.x)+(y - p.y)*(y - p.y));
} 		
