#include <iostream>

#include "nuple.hpp"

using namespace std;

int main()
{
	Nuple<int> n = {1,2,3,4};
	cout << n << endl;
}
