#include "array.hpp"
#include <iostream>

using namespace std;

int main()
{
	Array<int,5> a1;	
	cout << a1 << endl;

	Array<int,6> a2 = {1,2,3,4,5,6};
	cout << a2 << endl;

	Array<Array<int,5>,5> a2d;
	for(int i = 0; i < 5; i++)
		for(int j = 0; j < 5; j++)
			a2d[i][j] = i + j;

	for(int i = 0; i < 5; i++)
	{
		for(int j = 0; j < 5; j++)
			cout << a2d[i][j] << " ";
		cout << endl;
	}
}
