#include <iostream>
#include <vector>
#include <list>

using namespace std;

void increment(int & i)
{
	i++;
}

struct Printer
{
	template<class T>
	void operator()(const T& t)
	{
		cout << t << " ";
	}
};

template<class Container, class Function>
void foreach(Container& c, Function f)
{
	for(auto & i : c)
		f(i);
}

int main()
{
	Printer printer;

	list<int> l {1,2,3};
	foreach(l, increment);
	foreach(l, printer);
	cout << endl;
	foreach(l, [](int i) { cout << i << " "; });
	cout << endl;

	vector<int> v {1,2,3};
	foreach(v, increment);
	foreach(v, printer);
	cout << endl;
	foreach(v, [](int i) { cout << i << " "; });
	cout << endl;
}
