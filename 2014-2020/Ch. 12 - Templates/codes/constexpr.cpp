#include <iostream>
#include <stdexcept>

template<class Natural>
constexpr Natural factorial(Natural n)
{
    if(n < 0)
        throw std::out_of_range("Parameter must be positive");
    else if(n == 0 || n == 1)
        return 1;
    else
        return n * factorial(n - 1);
}

template<int n>
struct constN
{
    constN() { std::cout << n << std::endl; }
};

//source : cppreference
// constexpr functions signal errors by throwing exceptions
// in C++11, they must do so from the conditional operator ?:
/*
    constexpr char operator[](std::size_t n) const
    {
        return n < sz? p[n] : throw std::out_of_range("");
    }
*/

int main()
{
	constN<factorial(5)> out;
}
