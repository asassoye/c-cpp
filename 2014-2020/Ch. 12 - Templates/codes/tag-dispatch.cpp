#include <iostream>
#include <cmath>

using namespace std;

template<class T = double> constexpr T PI = T(atan(1) * 4);

struct AsRadius {};
struct AsDiameter {};

class Circle
{
	double radius;

	public:
    	explicit Circle(double radius, AsRadius) : radius(radius) {}
	    explicit Circle(double diameter, AsDiameter) : radius(diameter / 2) {}
	    
	    double area() { return PI<double> * radius * radius; }
};

int main()
{
	Circle c1(7, AsRadius());
	Circle c2(7, AsDiameter());
}
