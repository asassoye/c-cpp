#include <iostream>

using namespace std;

template<typename T>
T sum(T v) 
{
  return v;
}

template<class T, class ... Args>
T sum(T first, Args... args) 
{
  return first + sum(args...);
}

int main()
{
	int s = sum(1,2,3,4);
	cout << s << endl;

	string s1 = "a", s2 = "b", s3 = "c", s4 = "d";
	string str = sum(s1, s2, s3, s4);
	cout << str << endl;		

	//cout << sum("a", "b", "c", "d") << endl;
}
