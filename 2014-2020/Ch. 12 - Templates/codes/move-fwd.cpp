#include <iostream>

using namespace std;

void overloaded(const int & arg ) 
{ 
	cout << "by lvalue\n"; 
}

void overloaded(int && arg ) 
{ 
	cout << "by rvalue\n";
}
     
template<class T>
void forwarding( T && arg ) 
{
	cout << "by simple passing: ";
	overloaded(arg);
	cout << "via std::forward: ";
	overloaded( forward<T>(arg) );
	cout << "via std::move: ";
	overloaded( move(arg) ); //arg is now invalidated	
}

int main() 
{
	cout << "initial caller passes rvalue:\n";
	forwarding(5);
	cout << endl;

	cout << "initial caller passes lvalue:\n";
	int x = 5;
	forwarding(x);
}
