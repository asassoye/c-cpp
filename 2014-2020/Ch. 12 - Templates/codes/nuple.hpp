#ifndef NUPLE_H
#define NUPLE_H

#include <stdexcept>
#include <iostream>
#include <cstdlib>

using namespace std;

template<class T> class Nuple;
template<class T> std::ostream& operator<<(std::ostream&, const Nuple<T>&);

template<class T>
class Nuple
{
    const unsigned n;//nuple size
    T * nuple;//nuple components

    public:
        explicit Nuple(unsigned n);
        Nuple(unsigned n, T * t);       
        Nuple(std::initializer_list<T> l);

        ~Nuple();
        Nuple(const Nuple<T>& n);
        Nuple<T>& operator =(const Nuple<T>& n);

        inline T& operator[](unsigned i) const;
        inline unsigned size() const;

        friend std::ostream& operator<< <>(std::ostream& out, const Nuple<T>& n);
};

Nuple<int>::Nuple(unsigned n) : n(n), nuple()
{
	//do nothing
}

template<class T>
Nuple<T>::Nuple(unsigned n) : n(n), nuple(malloc(n * sizeof(T))) //ask why I malloc and not new of shared_ptr
{
    if(n == 0)
        throw std::range_error("Number of components of a nuple must be strictly positive");
}

template<class T>
Nuple<T>::Nuple(unsigned n, T * t) : n(n), nuple(t)
{
    if(n == 0)
        throw std::range_error("Number of components of a nuple must be strictly positive");
}

template<class T>
Nuple<T>::Nuple(std::initializer_list<T> l) : Nuple(l.size())
{
    unsigned i = 0;
    for(auto t : l)
    {
        nuple[i] = t;
        i++;
    }
}

template<class T>
Nuple<T>::~Nuple()
{
    if(nuple != nullptr)
	{ 
		free(nuple);
		nuple = nullptr;
	}
}

template<class T>
Nuple<T>::Nuple(const Nuple<T> &n) : Nuple(n.size())
{
    for(unsigned i = 0; i < n.size(); i++)
        nuple[i] = n[i];
}

template<class T>
Nuple<T>& Nuple<T>::operator =(const Nuple<T>& n)
{
    if(this != &n)
    {
        delete[] nuple;

        this->n = n.size();
        nuple = new T[this->n];
        for(unsigned i = 0; i < this->n; i++)
            nuple[i] = n[i];
    }
    return *this;
}

template<class T>
T& Nuple<T>::operator [](unsigned i) const
{
    return nuple[i];
}

template<class T>
unsigned Nuple<T>::size() const
{
    return n;
}

template<class T>
std::ostream& operator<<(std::ostream& out, const Nuple<T>& n)
{
    out << "( ";
    for(unsigned i = 0; i < n.size() - 1; i++)
        out << n[i] << " , ";
    out << n[n.size() - 1] << " )";

    return out;
}

#endif // NUPLE_H

