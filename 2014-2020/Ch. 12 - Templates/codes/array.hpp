#include <iostream>
#include <stdexcept>
#include <iostream>
#include <initializer_list>

using namespace std;

template<class T, int N> class Array;
template<class T, int N> std::ostream& operator<<(std::ostream&, const Array<T,N>&);

template<class T, int N> class Array
{
	T * t;

	public:
		Array() : t(new T[N]) {}
		Array(initializer_list<T> l) : Array<T,N>()
		{
			if(N != l.size())
				throw std::out_of_range("Non matching sizes");

			auto it = l.begin();
			for(unsigned i = 0; i < N; i++)
			{
				t[i] = *it;
				it++;
			}
		}

		~Array() { delete[] t; }

		Array(const Array<T,N>& a) = delete;
		Array<T,N>& operator=(const Array<T,N>& a) = delete;

		T& operator[](unsigned i) { return t[i]; }
		const T& operator[](unsigned i) const { return t[i]; }

		unsigned size() const { return N; }

        friend std::ostream& operator<< <>(std::ostream& out, const Array<T,N>& a);
};

template<class T, int N>
std::ostream& operator<<(std::ostream& out, const Array<T,N>& a)
{
    out << "{ ";
    for(unsigned i = 0; i < a.size() - 1; i++)
        out << a[i] << " , ";
    out << a[a.size() - 1] << " }";

    return out;
}
