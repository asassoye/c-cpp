#include <iostream>

using namespace std;

struct R {};

struct S 
{
	S()
	{
		cout << "+S" << endl;
	}
};

template<class T> struct A
{
	int i;
	T t;

	A(int i, T t) : i(i), t(t) { cout << "+A<T>" << endl; }			
};

template<> struct A<bool>
{
	S s;
	bool b;

	A(bool b) : b(b), s(S()) { cout << "+A<bool>" << endl; }
};

int main()
{
	A<R> a(2,R());
	cout << a.i << endl;
	
	A<bool> b(true);
	//cout << b.i << endl; //error : no A<bool>::i
	cout << b.b << endl;
}
