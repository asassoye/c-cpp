#include <iostream>

using namespace std;

/**
template <typename A, typename B, typename C>
void f(A& a, B& b, C& c)
{
    cout << a << " " << b << " " << c << endl;
}
*/

/**
template <typename A, typename B, typename C>
void f(const A& a, const B& b, const C& c)
{	
    cout << a << " " << b << " " << c << endl;
	//a = A(); b = B(); c = C();
}
*/

/*
template <typename A, typename B, typename C>
void f(const A& a, B& b, C& c);
template <typename A, typename B, typename C>
void f(A& a, const B& b, C& c);
template <typename A, typename B, typename C>
void f(A& a, B& b, const C& c);
template <typename A, typename B, typename C>
void f(const A& a, const B& b, C& c);
template <typename A, typename B, typename C>
void f(const A& a, B& b, const C& c);
template <typename A, typename B, typename C>
void f(A& a, const B& b, const C& c);
*/

template <typename A, typename B, typename C>
void f(A&& a, B&& b, C&& c)
{	
    cout << a << " " << b << " " << c << endl;	
}

//rule : if T is a reference, T&& is resoluted as T&, otherwise, std::move
int main()
{
	int i = 1;	
	double d = 2.5;
	char c = 'a';

	f(i, d, c);
	
	f(1, 2.5, 'a');

	int ii = 2;
	int j = std::move(ii);	
	cout << ii << endl; //seg fault
	cout << j << endl;
}
