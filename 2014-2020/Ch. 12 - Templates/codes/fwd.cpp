#include <iostream>

using namespace std;

/**
template<class Function, class ... Args>
auto call_function(Function f, Args&& ... args) -> decltype(f(args...))
{
	return f(args...);
}
*/

template<class Function, class ... Args>
auto call_function(Function f, Args&& ... args) -> decltype(f(args...))
{
	return f(forward<Args>(args)...);
}

void print(int i)
{
	cout << i << endl;
}

void increment(int & i)
{
	i++;
}

int main()
{
	call_function(print, 2);

	int i = 3;
	call_function(increment, i);
	print(i);

	//call_function(increment, 3); //we don't want this to compile : we have to use forward
	//with the first definition of call_function : undefined behaviour
}
