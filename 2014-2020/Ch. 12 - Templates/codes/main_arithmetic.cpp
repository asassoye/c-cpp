#include <iostream>
#include <iomanip>
#include <limits>

#include "arithmetic.hpp"

using namespace std;

template<int n>
struct constN
{
    constN() { std::cout << n << endl; }
};

int main()
{
	//cout << setprecision(std::numeric_limits<long double>::digits10 + 1);
	cout << setprecision(60);
	//cout << sizeof(double) << " " << sizeof(long double) << endl;
	//cout << std::numeric_limits<double>::digits << std::endl;
	//cout << std::numeric_limits<long double>::digits << std::endl;

	//cout << std::endl;

	cout << "PI double      = " << PI << endl;
	cout << "PI sans maj    = " << pi<> << endl;
	cout << "PI float       = " << pi<float> << endl;
	cout << "PI int         = " << pi<int> << endl << endl;	

	cout << "GCD(24,36) = " << gcd(24,36) << endl;
	cout << "5! = " << factorial(5) << endl;
	isPrime(97) ? cout << "97 is prime" : cout << "97 is not prime";
	cout << endl;
	cout << "3^9 = " << intPow(3,9) << endl;
	cout << "C^7_3 = " << binom(7,3) << endl;

	
	//constN<binom(7,3)> out; //not constexpr
	constN<intPow(3,9)> out;
	constN<isPrime(97)> out1;
	constN<factorial(5)> out2;
	constN<gcd(24,36)> out3;
	
}

