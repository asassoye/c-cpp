#include <iostream>

using namespace std;

template<class E,unsigned N>
class Array
{
	E* a;

	public:
		Array() : a(new E[N])
		{
		}	

		~Array()
		{
			delete[] a;
		}

		Array(const Array<E,N>& array) = delete;
		Array& operator=(const Array<E,N> array) = delete;

		inline E& operator[](unsigned i)
		{
			return a[i];
		}

		inline unsigned size() const 
		{ 
			return N; 
		}
};

int main()
{
	Array<int,5> a;

	for(int i = 0; i < a.size(); i++)
		a[i] = i + 1;
		
	for(int i = 0; i < a.size(); i++)
		cout << a[i] << " ";
	cout << endl;
}
