#include <iostream>

using namespace std;

struct A 
{
	void a() { cout << "A" << endl; }
};

struct B 
{ 
	int;
	void b() { cout << "B" << endl; }
};

template<class T>
bool isA(T t)
{
	return sizeof(t) == sizeof(A);
}

template<class T>
bool isB(T t)
{
	return sizeof(t) == sizeof(B);
}

template<class T>
void f(T t)
{
	if(isA(t))
		t.a();
	else
		t.b();
}

int main()
{
	A a; B b;
	
	f(a);
	f(b);
}
