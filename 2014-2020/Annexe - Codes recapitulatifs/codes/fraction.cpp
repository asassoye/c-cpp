#include <iostream>

using namespace std;

class A; //je dois savoir ce qu'est A pour la ligne 6
ostream& operator << (ostream&, const A&);//déclaration du prortotype << (pour la ligne 17)

struct A
{
    int i;

    A(int i) : i(i)
    {
        cout << "Cstr " << i << endl;
    }

    A(const A& a) : i(a.i) //je dois connaitre le prototype de <<
    {
        cout << "Cstr-c " << a << endl;
    }

    //friend ostream& operator << (ostream& out, const A& a);
};

void f(A a)
{
    cout << "f with " << a << endl;
}

ostream& operator << (ostream& out, const A& a)
{
    out << a.i;
    return out;
}
