#include <iostream>
#include <vector>
#include <functional>

using namespace std;

/**
//good, but we can do better
template<class Container, class Function>
void for_each(Container& c, Function f)
{
	for(auto elem : c)
		f(elem);
}
*/

template<template<class> class Container, class E, class ... Us, class Function>
void for_each(Container<E, Us ...>& c, Function f)
{
	for(E elem : c)
		f(elem);
}

//un foncteur pour imprimer
template<class T>
struct Printer
{
	void operator()(T t)
	{
		cout << t;
	}
};

//fonction indep pour imprimer des int
void int_print(int i)
{
	cout << i;
}

//fonction template indep pour imprimer
template<class T>
void print(T t)
{
	cout << t;
}

int main()
{
	vector<int> v = {1,2,3,4,5};	

	Printer<int> p;
	for_each(v, p);
	cout << endl;

	for_each(v, int_print);
	cout << endl;

	for_each(v, print<int>);
	cout << endl;

	//lambda
    //    liste de capture
	//          |
	//          |  params
	//          |    |
    //          |    |        Corps
    //          v    v    |            |
	for_each(v, [](int i) { cout << i; });
	cout << endl;

	vector<int> v2 = {5,4,3,2,1};
	//pas bien
	//for_each(v, [](int i) { cout << i; }); 
	//for_each(v2, [](int i) { cout << i; });
	auto f = [](int i) //-> void //précision du type de retour si compilateur pas content
	{
		cout << i;
	};
	for_each(v, f); cout << endl;
	for_each(v2, f);
	cout << endl;

	//à titre indicatif
	function<void (int)> f2 = [](int i) //classe de std::
	{
		cout << i;
	};
	for_each(v, f2); cout << endl;
	for_each(v2, f2);
	cout << endl;
}
