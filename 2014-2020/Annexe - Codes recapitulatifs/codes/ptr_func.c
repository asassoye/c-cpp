#include <stdio.h>
#include <stdbool.h>

void swap(int * i, int * j)
{
	int tmp = *i;
	*i = *j;
	*j = tmp;
}

bool int_greater(int i, int j)
{
	return i > j;
}

bool int_lower(int i, int j)
{
	return i < j;
}

bool int_cong3(int i, int j)
{
	return i % 3 > j % 3;
}

//f is a function returning bool and taking 2 int parameters
void insertionsort(int array[], int size, bool (*comp)(int,int))
{
	for(int i = 1; i < size; i++)
		for(int j = i; j > 0 && comp(array[j - 1], array[j]); j--)
			swap(&array[j], &array[j - 1]);
}

/*
void insertionsort(int array[], int size)
{
	for(int i = 1; i < size; i++)
		for(int j = i; j > 0 && array[j - 1] > array[j]; j--)
			swap(&array[j], &array[j - 1]);
}
*/

void print_array(int array[], int size)
{
	for(int i = 0; i < size; i++)
		printf("%d ", array[i]);
	printf("\n");
}

void print_array2(int * array, int size)
{
	int n = 0;
	int * pti = array;

	while(n < size)
	{
		printf("%d ", *pti);

		n++;
		pti++;
	}
	printf("\n");
}

void print_array3(int * array, int size)
{
	int n = 0;

	while(n < size)
	{
		printf("%d ", *(array + n));

		n++;
	}
	printf("\n");
}

void print_array4(void * array, int size, int size_of)
{
	int n = 0;

	while(n < size)
	{
		printf("%d ", *(int*)(array + n * size_of));

		n++;
	}
	printf("\n");
}

int main()
{
	/*
	int array[] = {2, 4, 6, 9, 10, 0, 8, 1, 7, 3, 5};
	insertionsort(array, 11);
	print_array(array, 11);
	*/

	int array2[] = {2, 4, 6, 9, 10, 0, 8, 1, 7, 3, 5};
	insertionsort(array2, 11, int_greater);
	print_array(array2, 11);

	int array3[] = {2, 4, 6, 9, 10, 0, 8, 1, 7, 3, 5};
	insertionsort(array3, 11, int_lower);
	print_array(array3, 11);

	int array4[] = {2, 4, 6, 9, 10, 0, 8, 1, 7, 3, 5};
	insertionsort(array4, 11, int_cong3);
	print_array(array4, 11);

	int brol;
	printf("address of brol        : %p\n", &brol);

	bool (*f)(int,int) = int_greater;
	printf("address of int_greater : %p\n", f);

	print_array2(array2, 11);
	print_array3(array2, 11);
	print_array4(array2, 11, sizeof(int));
}
