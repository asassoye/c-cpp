#include <iostream>
#include <vector>
#include <memory>

using namespace std;

struct Integer
{
	int val;

	Integer(int i = 0) : val(i) { cout << "+" << endl; }
	
	Integer(const Integer& i) : val(i.val) { cout << "r" << endl; }
	~Integer() { cout << "-" << endl; }

	Integer& operator=(const Integer& i)
	{
		if(&i != this)
			val = i.val;

		cout << "=" << endl;

		return *this;
	}

	operator int() { return val; }

	bool is_even() const { return val % 2 == 0; }
};

int main()
{
	vector<int> v1;
	cout << sizeof(v1) << endl;

	vector<int> v2(1000); //alloué sur le tas
	cout << sizeof(v2) << endl;

	//Integer i = 3;
	//cout << 3 + i << endl; 
	shared_ptr<Integer> pt = nullptr;
	//cout << pt->is_even() << endl; //seg fault : pt est nul
	pt = make_shared<Integer>(3);
	cout << pt->is_even() << endl;

	/*
     * Je vais allouer des objets sur le tas pour montrer que
     * vector / list / &co ne détruisent pas les objets pointés
	 */	

	vector<Integer*> v3;
	for(int i = 0; i < 5; i++)
		v3.push_back(new Integer(i));	
	
	for(Integer * i : v3)
	{
		delete i;
		i = nullptr;
	}

	cout << endl;

	//pure good alignment
	vector<shared_ptr<Integer>> v4;
	for(int i = 0; i < 5; i++)
		v4.push_back(make_shared<Integer>(i)); //make_shared est une fct
}
