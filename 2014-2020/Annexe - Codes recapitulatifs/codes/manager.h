#ifndef MANAGER_H
#define MANAGER_H

#include "departement.h" //include nécessaire si je dois connaitre la taille ou les opérations autorisées

class Manager
{
    Departement dep; //je dois connaitre la taille de Deparement

    public:
        Manager(Departement dep);
};

#endif // MANAGER_H
