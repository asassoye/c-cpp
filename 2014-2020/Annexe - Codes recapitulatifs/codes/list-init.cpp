#include <iostream>

using namespace std;

struct Brol
{
	Brol() = default;
	Brol(const Brol& b) { cout << "RB" << endl; }
	Brol& operator=(const Brol&) { cout << "=B" << endl; }
};

struct A_No_List
{
	Brol b;
	
	A_No_List(Brol b) { this->b = b; }
};

struct A_With_List
{
	Brol b;
	
	A_With_List(Brol b) : b(b) { }
};

struct Brol_No_Cstr_Def
{
	//no def cstr, because I have another cstr
	Brol_No_Cstr_Def(int) {}
	Brol_No_Cstr_Def(const Brol_No_Cstr_Def& b) { cout << "RB" << endl; }
	Brol_No_Cstr_Def& operator=(const Brol_No_Cstr_Def&) { cout << "=B" << endl; }
};

/*
struct No_List
{
	Brol_No_Cstr_Def b;

	No_List(Brol_No_Cstr_Def b)	{ this-> b = b; }
};
*/

struct With_List
{
	Brol_No_Cstr_Def b;

	With_List(Brol_No_Cstr_Def b)	: b(b) {}
};

/*
struct No_List2
{
	Brol& b;
	const Brol cb;

	No_List2(Brol & b, Brol cb)
	{
		this->b = b;
		this->cb = cb;
	}
};
*/

struct With_List2
{
	Brol& b;
	const Brol cb;

	With_List2(Brol & b, Brol cb) : b(b), cb(cb) {}
};

int main()
{
	//1. with or without init list lead to different behaviours
	Brol brol;
	
	A_No_List a1(brol);
	cout << endl;

	A_With_List a2(brol);
	cout << endl;

	//2. without init-list, we cannot affect attributes without default constructors
	Brol_No_Cstr_Def bb(2);
	//No_List nl(bb);

	With_List wl(bb);

	//3. without initlist, we cannot affect constant attributes (const and refs)
	//No_List2 nl2(brol, brol);	
	With_List2 wl2(brol, brol);
}
