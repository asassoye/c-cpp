#include <iostream>

using namespace std;

struct A 
{ 
	int i;
	A(int i = 0) : i(i) {}
};

struct B
{
	A * a;
	B(int i) : a(new A(i)) {}
	~B() { delete a; }
	void print() {	cout << (*a).i << endl;  }	
};

struct Tab
{
	int * pt;
	int t;
	Tab(int i) : pt(new int[i]), t(i) 
	{
		for(int i = 0; i < t; i++)
			pt[i] = i;
	}

	~Tab() { delete[] pt; }
	
	void print() 
	{	
		for(int i = 0; i < t; i++)
			cout << pt[i] << " ";
		cout << endl;
	}	
};

void f(B b) { }
void f(Tab t) { }

int main()
{
	B b(1);
	f(b);
	b.print();	
	
	B bb(2);
	bb = b;
	bb.print();
	
	bb = bb;
	bb.print();

	cout << endl;

	Tab t(5);
	t.print();
	f(t);
	t.print();
}
