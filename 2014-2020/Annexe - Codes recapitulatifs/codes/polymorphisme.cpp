#include <iostream>
#include <memory>

using namespace std;

class A
{
	protected:
		int i;
	public:
		A(int i = 0) : i(i) {}
		virtual ~A() { cout << "-A" << endl; }
		//void print() { cout << i << endl; } //pas de polymorphisme
		virtual void print() { cout << i << endl; }		
};

class B : public A
{
	protected:
		int j;
	public:
		B(int j = 0) : A(j + 2), j(j - 1) {}
		virtual ~B() { cout << "-B" << endl; }
		void print() { cout << (i + j) << endl; }	
};

int main()
{
	//sans plymorphisme
	A a = B(3);
	a.print(); // 5

	//avec polymoprhisme, méthode 1
	B b(3);
	A & ra = b;
	ra.print();

	//méthode 2
	A * pta = new B(3);
	pta->print();

	//méthode 3
	shared_ptr<A> spta = std::make_shared<B>(3);
	spta->print();	
}
