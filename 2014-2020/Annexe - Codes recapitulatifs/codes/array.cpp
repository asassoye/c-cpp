#include "array.h"

Array::Array(unsigned size) : data(new int[size]), _size(size) 
{

}

Array::~Array()
{
	if(data != nullptr)
	{
		delete[] data;
		data = nullptr;

		_size = 0;
	}
}

Array::Array(const Array& array) 
{
    delete[] data;   
    
    data = new int[array._size];
    _size = array._size;
    
    for(unsigned i = 0; i < _size; i++)
        data[i] = array[i];
}

Array& Array::operator=(const Array& array)
{
    if(this != &array)
    {
        delete[] data;   
    
        data = new int[array._size];
        _size = array._size;
    
        for(unsigned i = 0; i < _size; i++)
            data[i] = array[i];
    }
    
    return *this;
}

ArrayIterator Array::begin() const
{
	return ArrayIterator(*this, 0);
}

ArrayIterator Array::end() const
{
	return ArrayIterator(*this, _size);
}

std::ostream& operator<<(std::ostream& out, const Array& array)
{
	out << "{ ";

	for(unsigned i = 0; i < array._size - 1; i++)
		out << array[i] << " , ";			
	out << array[array._size - 1] << " }";

	std::cout << "end" << std::endl;
	
	return out;	
}

ArrayIterator::ArrayIterator(const Array& array, int i) : array(array), i(i){}

ArrayIterator& ArrayIterator::operator++()
{
	if(i != array.size())
		i++;

	return *this;
}

const int& ArrayIterator::operator*() const
{
	return array[i];
}

bool ArrayIterator::operator!=(const ArrayIterator& it) const
{
	//std::cout << current << " " << it.current << std::endl;
	//return current != it.current;
	//return array != it.array || 
	if(&array != &it.array)
		return true;
	else
		return i != it.i;
}
