#include <iostream>
#include <vector>
#include <stdexcept>
#include <initializer_list>

template<class T>
class vector
{
	std::vector<T> comps;

	public:
		//construit un vecteur nul
		explicit vector(int size) : comps(std::vector<T>(size)) 
		{
			if(size <= 0)
				throw std::invalid_argument("Size must be stricly positive");
		}

		vector(std::initializer_list<T> l) : comps(std::vector<T>(l.size()))
		{
			int i = 0;
			for(T t : l)
			{
				comps[i] = t;
				i++;
			}
		}

		//pas surchargé << parce que, pour l'exemple, ça va me demander de faire des teplates amis
        //et c'est un peu pénible
		void print()
		{
			std::cout << "(";
			for(int i = 0; i < comps.size() - 1; i++)
				std::cout << comps[i] << " , ";

			std::cout << comps[comps.size() - 1] << ")";
		}

		unsigned size() const
		{
			return comps.size();
		}

		vector<T> operator +(const vector<T>& v) const
		{
			std::cout << v.size() << std::endl;
			if(size() != v.size())
				throw std::invalid_argument("Vectors must have the same size");

			vector<T> result(size());
			for(int i = 0; i < size(); i++)
				(result.comps)[i] = comps[i] + (v.comps)[i];

			return result;
		}
};

int main()
{
	vector<int> v1(3);
	v1.print();
	std::cout << std::endl;

	vector<int> v2 = {1,2,3};
	vector<int> v3 = {4,5,-1};
	vector<int> sum = v2 + v3;
	std::cout << sum.size() << std::endl;
	sum.print();
}
