#include <iostream>
#include <initializer_list>

using namespace std;

class Dataset
{
	double _sum;
	unsigned n;

	public:
		Dataset() : _sum(0), n(0) {}

		void feed(double d)
		{
			_sum += d;
			n++;
		}		


		void feed(initializer_list<double> l)
		{
			for(double d : l)
				feed(d);
		}
		
		double sum() const { return _sum; }
		unsigned count() const { return n; }
};

int main()
{
	Dataset set;
	cout << set.sum() << endl;
	cout << set.count() << endl;

	set.feed(2);
	set.feed({4,5,6});	

	cout << set.sum() << endl;
	cout << set.count() << endl;
}
