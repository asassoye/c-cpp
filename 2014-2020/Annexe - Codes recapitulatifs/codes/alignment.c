#include "stdio.h"

struct One
{
	char i;
};

struct Four
{
	int i;
};

struct HaHa
{
	char c;
	int i;	
};

int main()
{
	printf("%zu\n", sizeof (struct One));
	printf("%zu\n", sizeof (struct Four));
	printf("%zu\n", sizeof (struct HaHa));
}
