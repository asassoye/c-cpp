#include <iostream>
#include "array.h"

using namespace std;

int main()
{
	//Array a = 5;
	Array a(5);
	for(int i = 0; i < 5; i++)
		a[i] = i * i;

	cout << a << endl;

	for(int i : a)
		cout << i << endl;	

	auto it = a.begin();
	auto end = a.end();

	int i = 0;
	while(it != end)
	//while(i <= 8)
	{
		//cout << i << " " << *it << endl;
		cout << *it << " ";
		++it;
		i++;
	}

	//for(int i : a)
	//	cout << i << " ";
	cout << endl;
}
