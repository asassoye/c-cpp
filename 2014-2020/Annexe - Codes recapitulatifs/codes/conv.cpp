#include <iostream>
#include <initializer_list>

using namespace std;

struct B
{
	//explicit B(int) { cout << "+B" << endl; }
	B(int) { cout << "+B" << endl; }

	//B(initializer_list<int> l) { cout << "lB" << endl; }

	//B(const B& b) = delete;
	B(const B& b) { cout << "rB" << endl; }

	B& operator=(const B& b) = delete;

	/*
	B& operator=(const B& b)  //never called
	{ 
		cout << "=B" << endl;
		return *this;
	}
	*/
};

int main()
{
	B b1 = 3; //cstr implicit, appel potentiel au cstr recop
	B b2 = {3}; //cstr implicit, pas de conv sur params, si std::init_list, on appelle
	B b3(3);
	B b4 {3}; //pas de conv sur params, si std::init_list, on appelle
	B b5 = B(3); //appel potentiel au cstr recop
}
