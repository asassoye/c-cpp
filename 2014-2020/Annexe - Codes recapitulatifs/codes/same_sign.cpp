#include <iostream>
#include <type_traits>

template<class T>
using isNotFloat = std::enable_if_t<! std::is_floating_point_v<T>>;

template<class T>
using isFloat = std::enable_if_t<std::is_floating_point_v<T>>;

template<class Number, class = isNotFloat<Number>>
bool same_sign(Number n1, Number n2, int)
{
	return (n1 ^ n2) >= 0;
}

template<class Number, class = isFloat<Number>>
bool same_sign(Number n1, Number n2, long)
{
	return (n1 >= 0 && n2 >= 0) || (n1 < 0 && n2 < 0);
}

template<class Number>
bool same_sign(Number n1, Number n2)
{
	return same_sign(n1, n2, 0);
}

int main()
{
	//signed integers
	std::cout << same_sign(8,9) << std::endl;
	std::cout << same_sign(-8,9) << std::endl;
	std::cout << same_sign(8,-9) << std::endl;
	std::cout << same_sign(-8,-9) << std::endl << std::endl;

	std::cout << same_sign(8,999) << std::endl;
	std::cout << same_sign(-8,999) << std::endl;
	std::cout << same_sign(8,-999) << std::endl;
	std::cout << same_sign(-8,-999) << std::endl << std::endl;

	//unsigned integers
	unsigned i = 8; unsigned j = 9;	
	std::cout << same_sign(i, j) << std::endl;
	i = 999;
	std::cout << same_sign(i, j) << std::endl << std::endl;

	//Floats
	std::cout << same_sign(8.0,9.0) << std::endl;
	std::cout << same_sign(-8.0,9.0) << std::endl;
	std::cout << same_sign(8.0,-9.0) << std::endl;
	std::cout << same_sign(-8.0,-9.0) << std::endl << std::endl;

	std::cout << same_sign(8.0,999.0) << std::endl;
	std::cout << same_sign(-8.0,999.0) << std::endl;
	std::cout << same_sign(8.0,-999.0) << std::endl;
	std::cout << same_sign(-8.0,-999.0) << std::endl;
}
