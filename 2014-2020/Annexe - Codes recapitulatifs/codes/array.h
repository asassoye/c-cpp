#ifndef ARRAY_H
#define ARRAY_H

#include <iostream>
#include <stdexcept>

class ArrayIterator;

class Array
{
    int* data;
    unsigned _size;
    
    public:
        explicit Array(unsigned size);
		
		~Array();
		Array(const Array& array);
		Array& operator=(const Array& array);
        
        inline unsigned size() const;
        inline int& operator[](unsigned index);            
		inline const int& operator[](unsigned index) const;

		ArrayIterator begin() const;
		ArrayIterator end() const;

		friend std::ostream& operator<<(std::ostream& out, const Array& array);
};

class ArrayIterator
{
	const Array& array;
	int i;

	public:
		ArrayIterator(const Array& array, int i);
		ArrayIterator& operator++();
		const int& operator*() const;		
		bool operator!=(const ArrayIterator& it) const;
};


unsigned Array::size() const
{
	return _size;
}

int& Array::operator[](unsigned index)
{
	if(index < 0 || index >= _size)
		throw std::out_of_range("Out of bounds");

	return data[index];
}

const int& Array::operator[](unsigned index) const
{
	if(index < 0 || index >= _size)
		throw std::out_of_range("Out of bounds");

	return data[index];
}

#endif
