#include <iostream>

using namespace std;

struct A
{
	int i;
	//A(int i) : i(i) { cout << "Build a A(" << i << ")" << endl; }
	explicit A(int i) : i(i) { cout << "Build a A(" << i << ")" << endl; } //pas de conversions

	//A operator + (A a) { return A(i + a.i); } //sol 1	

	A& operator=(const A& a) { cout << "=" << endl; i = a.i; return *this; }//never called
};

A operator + (A a1, A a2) //sol 2
{
	return A(a1.i + a2.i);
}

A operator +(A a, int i) //variante explicit
{
	return a + A(i);
}

A operator +(int i, A a) //variante explicit
{
	return a + A(i);
}

int main()
{
	A a(1);
	A b(2);
	A c = a + b; //sol 1,2 ok
	A d = a + 1; //sol 1,2 ok, explicit sans variante ko
	A e = 1 + a; //sol 2 ok, explicit sans variante ko
	cout << c.i << endl;
}
