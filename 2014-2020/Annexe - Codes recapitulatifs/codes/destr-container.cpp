#include <iostream>
#include <vector>

using namespace std;

struct A 
{
	~A() { cout << "Boom" << endl; }
};

struct B
{
	vector<A*> v;
	B() : v(vector<A*>(4, new A)) {}
	~B()
	{
		for(A* a : v)
		{	
			delete a;
			a = nullptr;
		}		
		v.clear();
	}
};

void f() { B b; }

int main()
{
	f();
	cout << "I want you in my room" << endl << "(outdated song)" << endl;
}
