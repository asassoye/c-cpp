#ifndef DEPARTEMENT2_H
#define DEPARTEMENT2_H

//Hygiène de programmation
//utiliser déclarations anticipées quand on peut, les include quand on doit

class Manager2;
//#include "manager2.h"

class Departement2
{
    Manager2* mgr;

    public:
        Departement2(Manager2& mgr);
};

#endif // DEPARTEMENT2_H
