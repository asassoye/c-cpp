#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct LinkedList
{
	struct node * head;
	struct node * tail;
	int size;
};

typedef struct LinkedList LinkedList;

LinkedList* make_list() 	
{	
	LinkedList * l = (LinkedList*)malloc(sizeof(LinkedList));
	l->head = NULL;
	l->tail = NULL;
	l->size = 0;

	return l;
}

//LinkedList * l2 = NULL;
//make_list2(l2); //purpose : allocate l2, now l2 points correctly
void make_list2(LinkedList* l)
{
	l = (LinkedList*)malloc(sizeof(LinkedList));
	l->head = NULL;
	l->tail = NULL;
	l->size = 0;
}

void make_list3(LinkedList** l) //passing the address of a pointer
{
	*l = (LinkedList*)malloc(sizeof(LinkedList));
	(*l)->head = NULL;
	(*l)->tail = NULL;
	(*l)->size = 0;
}

int list_size(struct LinkedList l)
{
	return l.size;
}

void list_print(struct LinkedList l)
{
	printf("{ ");
	for(struct node * current = l.head; current != NULL; current = current->next)
		printf("%d ", current->data);
	printf("}\n");
}

int main()
{
	LinkedList * l = make_list();
	printf("l : %p %p %d\n", l->head, l->tail, l->size);
	list_print(*l);

	//LinkedList * l2 = NULL;
	//make_list2(l2); //purpose : allocate l2, now l2 points correctly
	//printf("l2 : %p %p %d\n", l2->head, l2->tail, l2->size);
	//list_print(*l2);

	LinkedList * l3 = NULL;
	make_list3(l3); //purpose : allocate l3, now l3 points correctly
	printf("l3 : %p %p %d\n", l3->head, l3->tail, l3->size);
	list_print(*l3);
}
