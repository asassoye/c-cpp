#include <stdio.h>

void increment_val(int i)//i passed by value
{
	i++;
}

void increment_adr(int * i)//i passed by adress
{
	(*i)++;
}

int main()
{
	int i = 2; //0xCAFE
	increment_val(i);
	
	printf("%d\n",i);

	increment_adr(i);
	printf("%d\n",i);
}
