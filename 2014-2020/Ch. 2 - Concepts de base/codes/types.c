#include <stdio.h>

int main()
{
	int i = 2;
	printf("%d\n",i);

	double f1 = 2.0;
	double f2 = 2.1;
	printf("%f %f\n",f1,f2);

	double * ptf1 = &f1; //pointer
	printf("%p\n", ptf1); //prints adress
	printf("%f\n", *ptf1); //prints data

	f1 += 0.5;
	printf("%f\n", *ptf1);	
}
