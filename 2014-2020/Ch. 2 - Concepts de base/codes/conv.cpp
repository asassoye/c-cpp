#include <iostream>

using namespace std;

int main()
{
	int i1 = 3;
	int i2 = 2.5;
	double d1 = i1;
	double d2 = 2.5;
	int i3 = d2;
	int i4 = 2.5f;

	cout << i1 << endl;
	cout << i2 << endl;
	cout << d1 << endl;
	cout << d2 << endl;
	cout << i3 << endl;
	cout << i4 << endl << endl;

	cout << i1 / i3 << endl << endl;

	int p = 2 * 3L + 5.0F;

	cout << (0 == true) << endl;
	cout << (1 == true) << endl;
	cout << (2 == true) << endl;
}
