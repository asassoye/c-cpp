#include <iostream>

using namespace std;

namespace nbrol
{
	void f1();

	namespace nsubbrol
	{
		void f1();
		void f2();
	}
}

namespace nbrol
{
	void f1()
	{
		cout << "nbrol::f1" << endl;
	}
}

namespace nbrol
{
	namespace nsubbrol
	{
		void f1()
		{
			cout << "nsubbrol::f1" << endl;
		}

		void f2()
		{
			cout << "nsubbrol::f2" << endl;
		}
	}
}

int main()
{
	//f1();
	nbrol::f1();	
	nbrol::nsubbrol::f1();

	//nbrol::f2();	
	nbrol::nsubbrol::f2();
}
