#include <stdio.h>
#include <string.h>

int main()
{
	char c[20] = "salut petit poilu !"; //19 + \0
	printf("%d\n", (int)strlen(c));

	char c2[] = "coucou";
	for(int i = 0; i < 6; i++)//try with 7
		c[i] = c2[i];
	printf("%d\n", (int)strlen(c));

	char c3[] = "salut petit poilu !";
	strcpy(c3, c2);
	printf("%d\n", (int)strlen(c3));

	char c4[strlen(c2) + strlen(c3) + 1]; //+1 for \0
	strcpy(c4, c3);
	strcat(c4, c2); 
	printf("%s\n", c4);
}
