#include <iostream>
#include <vector>
#include <stdexcept>

using namespace std;

int main()
{
	vector<int> v1;

	for(int i = 0; i < 10; i++)
		v1.push_back(i * i);

	for(int i = 0; i < v1.size(); i++)
		cout << v1[i] << endl;

	///////
	cout << endl;

	vector<int> v2 = {1, 2, 3, 4, 5};

	for(int i : v2)
		cout << i << endl;

	try
	{
		v2.at(3000) = 0;
	}
	catch(out_of_range& ex)
	{
		cout << "you're out with at" << endl;
	}

	try
	{
		v2[30] = 0;
	}
	catch(...)//catch whatever can be caught
	{
		cout << "you're out with []" << endl;
	}
}
