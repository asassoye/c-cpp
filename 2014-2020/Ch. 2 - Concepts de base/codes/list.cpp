#include <iostream>
#include <list>
#include <stdexcept>
#include <typeinfo>


using namespace std;

int main()
{
	list<int> l1;

	for(int i = 0; i < 10; i++)
		l1.push_back(i * i);

	for(int i : l1)
		cout << i << endl;

	///////
	cout << endl;

	list<int> l2 = {1, 2, 3, 4, 5};

	auto it = l2.begin();
	cout << typeid(it).name() << endl;

	while(it != l2.end())
	{
		cout << "At " << &(*it) << " I had " << *it << " and now I have " << (*it+5) << endl;
		*it = *it + 5;
		it++;//try to remove this
	}

	for(int i : l2)
		cout << i << endl;
}
