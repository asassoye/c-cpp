#include <stdio.h>

unsigned mystrlen(const char * str)
{
	unsigned len = 0;
	while(*str != '\0') // != 0
	{
		printf("%d|", (int)*str);
		len++;
		str++; //j'avance le pointeur str de 1 fois la taille d'un char
		//str = str + 1		
	}
	printf("%d\n", (int)*str);

	return len;
}

int main()
{
	const char * str1 = "Salut";
	printf("%d\n", mystrlen(str1));

	const char str2[6] = "Salut";
	printf("%d\n", mystrlen(str2));

	const char str3[5] = "Salut"; //undefined behaviour
	printf("%d\n", mystrlen(str3));	
}
