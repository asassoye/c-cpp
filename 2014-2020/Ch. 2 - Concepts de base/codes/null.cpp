#include <iostream>

using namespace std;

void f(int i)
{
	cout << (2 * i) << endl;
}

int main()
{
	cout << "Hello world!" << endl;

	f(2);
	//f(nullptr);
	f(NULL);

	int i = 2;
	int * addr_i = &i;

	cout << addr_i << endl;
	cout << *addr_i << endl;

	addr_i += 2;
	cout << addr_i << endl;
	cout << *addr_i << endl;
	cout << sizeof(int) << endl;

}
