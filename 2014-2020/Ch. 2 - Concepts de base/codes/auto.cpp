#include <iostream>
#include <cmath>
#include <typeinfo>

using namespace std;

auto f(int i)
{
	switch(i)
	{
		case 1 : return sqrt(i);
		//case 2 : return "No";
		case 2 : return cos(i);
		//default: return i; //uncomment	
		default : return (double)i;
	}
	
}

//pipe output to c++filt -t
int main()
{
	auto a = 3 + 4;
	cout << a << " of type " << typeid(a).name() << endl;

	//auto c; //ko
	decltype(a) b;
	cout << b << endl; //undefined
	b = 7.2;
	cout << b << " of type " << typeid(b).name() << endl;
	cout << endl;

	for(int i = 0; i <=3; i++)
		cout << f(i) << " of type " << typeid(f(i)).name() << endl;

	return 1;

	auto l = {1, 2};
	cout << "Type of l : " << typeid(l).name() << endl;
}
