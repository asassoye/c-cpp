#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	char s1[6] = "salut"; //6, not 5
	char s2[20] = "salut";
	printf("%d\n", strlen(s1));
	printf("%d\n", strlen(s2));
	
	char * s3 = "salut";	
	char * s4 = malloc( sizeof(char) * 20);
	
	printf("%d\n", strlen(s3));
	printf("%d\n", strlen(s4));
	
	for(int i = 0; i < strlen(s3); i++)
		s4[i] = s3[i];
		
	printf("%d\n", strlen(s3));
	printf("%d\n", strlen(s4));
}
