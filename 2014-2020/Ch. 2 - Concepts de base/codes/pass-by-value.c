#include <stdio.h>

/**
void f(int a)
{
	printf("a : %p\n", &a);
}
*/

void f(int* pta)
{
	printf("    a : %d\n", *pta);
	printf("  pta : %p\n", pta);
	printf("& pta : %p\n", &pta);
}

int main()
{
	int i = 2;
	printf("    i : %p\n", &i);
	//f(i);

	int * pti = &i;
	printf("  pti : %p\n", pti);
	printf("& pti : %p\n", &pti);
	f(pti);
}
