#include <stdio.h>

#include <math.h>

#define PI (atan(1) * 4)

int main()
{
	printf("%-16s : %f\n", "Default PI", PI);
	printf("%-16s : %.10f\n", "10-digits PI", PI);
	printf("%-16s : %.20f\n", "20-digits PI", PI);
	
	//printf("%-60s|\n", " ");
	//printf("%-16s : %.60f\n", "20-digits PI", PI);

	int n = 42;
	printf("42 - base 8 : %o\n", n);
	printf("42 - base 10 : %d\n", n);
	printf("42 - base 16 : %x\n", n);
}
