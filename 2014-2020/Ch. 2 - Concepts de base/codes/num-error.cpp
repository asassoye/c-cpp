#include <iostream>
#include <limits>

using namespace std;

int main()
{
	int x = numeric_limits<int>::max();
	cout << x << endl;
	x+=x;//overflow
	cout << x << endl << endl;

	float y = 1E30;
	float div = y;
	cout << y << endl;
	//cout << y / y / y / y << endl << endl;//underflow
	while(y != 0)
	{
		y /= div;
		cout << y << endl;
	}

	cout << endl;
	
	double zero = 0;
	cout << x / zero << endl;//div-0

	int zzero = 0;
	cout << x / zzero << endl;//div-0
}
