#include <stdio.h>

void print_int_tab(int * tab, int size)
{
	for(int i = 0; i < size; i++)
		printf("%d ", tab[i]);
	printf("\n");
}

void print_str_tab(const char * tab)
{
	const char * ptr = tab;
	while(*ptr != '\0')
	{
		printf("%c", *ptr);

		ptr++;
	}
	printf("\n");
}

long unsigned sneaky(char array[])
{
	return sizeof(array) / sizeof(*array);
}

int main()
{
	int tab[] = {1,2,3,4};
	print_int_tab(tab, 4);

	const char* str = "coucou";
	print_str_tab(str);	
		
	char array[7];
	printf("%lu\n", 	sizeof(array) / sizeof(*array));
	printf("%lu\n", 	sneaky(array));
}
