#include <iostream>

using namespace std;

int main()
{
	int i = 2;
	cout << i << endl;

	double f1 = 2.0;
	double f2 = 2.1;
	cout << f1 << endl;
	cout << f2 << endl;

	double * ptf1 = &f1; //pointer
	cout << ptf1 << endl; //print adress
	cout << *ptf1 << endl; //print data
	f1 += 0.5;
	cout << *ptf1 << endl;

	//C++ seulement
	int & ri = i; //reference
	cout << ri << endl; //prints 'i'
	i++;
	cout << ri << endl; 
}
