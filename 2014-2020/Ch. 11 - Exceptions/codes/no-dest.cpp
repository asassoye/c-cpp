#include <iostream>
#include <stdexcept>
#include <memory>

using namespace std;

struct A 
{
	A(int i) { cout << "+A " << i << endl; }
	
	~A()
	{
		cout << "-A" << endl;
	}
};

int main()
{
	try
	{
		//A * a = new A();
		auto smart = make_shared<A>(3);
		throw 0;
	}
	catch(const int & i)
	{
		cout << "Error" << endl;
	}
}
