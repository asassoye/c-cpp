#include <iostream>
#include <climits>

using namespace std;

constexpr int sizeofmb = 1E6 * CHAR_BIT / 8;

int main()
{		
	long long unsigned int j = 0;
	
	bool exit = false;
	while(! exit)
	{
		cout << "Type the amounf of memory (in Mb) to allocate (negative number to close)" << endl;
		int i = -1; cin >> i;

		if(i <= 0)
			exit = true;
		else
		{
			new int[i * sizeofmb];
			j += i;

			cout << "Allocated " << j << " Mb in total" << endl;
		}
	}
}	
