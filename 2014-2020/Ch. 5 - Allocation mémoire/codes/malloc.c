#include <stdio.h>
#include <stdlib.h>

int main()
{
	void* ptv = malloc(sizeof(int));
	
	int* pti = (int*)ptv;

	printf("%d\n",*pti);

	*pti = 3;

	printf("%d\n",*pti);

	free(pti);
	//free(pti);
	//printf("%d\n", *pti);

	///////////////////////

	int * pt;

	//printf("%p\n",pt);
	printf("%d\n",*pt);

	*pt = 3;

	printf("%d\n",*pt);
}
