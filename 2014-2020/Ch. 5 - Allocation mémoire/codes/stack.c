#include <stdio.h>
#include <stdlib.h>

int main()
{
	printf("Size of int : %lu\n", sizeof(int));

	int tab[] = {1, 2, 3, 4, 5};

	int x = 174;
	int y = 200;
	int z = 42;

	printf("  adress of tab = %p \n", &tab);
	printf("x = %3d, adress = %p \n", x, &x);
	printf("y = %3d, adress = %p \n", y, &y);	
	printf("z = %3d, adress = %p \n", z, &z);		

	int rdm;
	printf("%d\n",rdm);

	int* it = tab;
	for(int i = 0; i <= 5; i++)
	{
		printf("%d ", *it);
		it++;
	}
	
}
