#include <iostream>

using namespace std;

struct A
{
	int * tab;
	int n;

	A(int n) : tab(new int[n]), n(n) {}	
	~A() { delete[] tab; }

	A(const A& a) = delete;

	void print()
	{
		for(int i = 0; i < n; i++)
			cout << tab[i] << " ";
		cout << endl;
	}
};

void f(A a) {}

int main()
{
	A a(5);
	for(int i = 0; i < 5; i++)
		a.tab[i] = i + 1;

	f(a);
	a.print();
}

//1 2 3 4 5
//Segmentation fault
