#include <iostream>
#include <vector>
#include <limits>

#include <stdlib.h>
#include <stddef.h>

using namespace std;

//heap size : ulimit -s = 8192 kb

class BigStuff
{
	vector<char> v;

	public:
		BigStuff(int size) : v(vector<char>(size * 1000))
		{
			for(int i = 0; i < v.size(); i++)
				v[i] = 'c';
		}
};

int main()
{	
	//BigStuff * b1 = new BigStuff(2500);//~ 5692 left

	//BigStuff * b2 = new BigStuff(4500);//~ 1192 left

	//delete b1;//3692 left		

	//BigStuff * b3 = new BigStuff(3000);//ko : 	

	//BigStuff b;
	//cout << sizeof(b) << endl;

	//char * c = new char[2500];
	//cout << sizeof(*c) << endl;

	//cout << numeric_limits<int>::max() << endl;

	/*
	size_t n1 = 1500ul * 1000000ul;//1.5Gb
	void* p1 = malloc(n1);

	size_t n2 = 3500ul * 1000000ul;//3.5Gb
	void* p2 = malloc(n2);

	//free(p1);
	size_t n3 = 2000ul * 1000000ul;//2Gb
	void* p3 = malloc(n3);
	*/

	
	long long unsigned int j = 0;
	long long unsigned int m5GB = 6000000;
	while(true)
	{
		new int[250];//1kb
		j++;
		if(j % 100 == 0)
			cout << j << "kb allocated" << endl;		
		if(j >= m5GB)
			break;
	}
	

//	new int[250 * 1000];

	int i;
	cout << "Type sth to close" << endl;
	cin >> i;
}	
