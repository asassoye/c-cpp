#include <iostream>
#include <random>

using namespace std;

int ran_int()
{
	static random_device rd;
	static mt19937 gen(rd());
	static uniform_int_distribution<> dis;

	return dis(gen);
}

int main()
{
	for(int i = 0; i < 20 ; i++)
		cout << ran_int() << " ";
	cout << endl;
}
