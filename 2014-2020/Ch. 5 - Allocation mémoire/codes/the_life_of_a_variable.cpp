#include <iostream>

using namespace std;

int * ptri = nullptr;
int * ptrj = nullptr;
int * ptrk = nullptr;

int global = 2;

void f()
{
	int j = 3;
	ptrj = &j;
	
	//cout << i << endl;
	cout << *ptri << endl;
	cout << j << endl;
	cout << *ptrj << endl;
	//cout << k << endl;
	cout << *ptrk << endl;
	cout << global << endl;
}

int main()
{
	//try to put f here

	int k = 1;
	ptrk = &k;

	//try to put f here

	//declare a block
	{
		static int i = 4;
		ptri = &i;
	}

	f();

	//cout << i << endl;
	cout << *ptri << endl;
	//cout << j << endl;
	cout << *ptrj << endl;
	//cout << k << endl;
	cout << *ptrk << endl;
	cout << global << endl;
}
