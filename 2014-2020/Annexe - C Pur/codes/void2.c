#include <stdio.h>

int main()
{
	int x = 2;
	void* pt1;
	printf("%d\n",sizeof (int));
	printf("%d\n", sizeof (void*));
	printf("%d\n",sizeof pt1);
	printf("\n");

	pt1 = &x;
	printf("%d\n", sizeof (void*));
	printf("%d\n",sizeof pt1);

	void * pt2 = &x;
	printf("%d\n", sizeof (void*));
	printf("%d\n",sizeof pt2);
}
