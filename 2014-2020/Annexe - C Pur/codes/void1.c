#include <stdio.h>

struct enreg
{
	int num;
	int amount;
	float prix;
};

int main()
{
	struct enreg e;//try without struct
	e.num = 1;
	e.amount = 2;
	e.prix = 2.5;

	void* pt = &e;

	int i = *((int*)pt);//ole
	printf("%d\n",i);
	
	pt+=sizeof(int);
	i = *((int*)pt);
	printf("%d\n",i);

	pt+=sizeof(int);
	float f = *((float*)pt);
	printf("%f\n",f);
}
