#include <stdio.h>
#include <stdlib.h>

struct node
{
	int data;
	struct node * next;
};

struct list
{
	struct node * first;
	struct node * last;
};

void print(struct list l)
{
	struct node * current = l.first;
	
	while(current != 0)
	{
		printf("%i", current->data);
		current = current->next;
	}
}

int length(struct list l)
{
	int length = 0;
	struct node * current = l.first;
	
	while(current != 0)
	{
		length++;
		current = current->next;
	}

	return length;
}

void destroy(struct list l)
{
	struct node * current = l.first;
	
	while(current != 0)
	{
		struct node * tmp = current;
		current = current->next;
		free(tmp);
	}
}

void init(struct list l)
{
		printf("helloooo");
	l.first = 0;
	l.last = 0;
		printf("hellowwww");
}

void add(struct list l, int data)
{
	struct node * toAdd = malloc(sizeof(struct node));
	toAdd->data = data;
	toAdd->next = 0;

	printf("hello");

	if(l.first == 0)
	{	printf("hello1");
		l.first = toAdd;
		l.last = toAdd;
	}
	else
	{	printf("hello2");
		(l.last)->next = toAdd;
		l.last = toAdd;
	}
}

int main()
{
	struct list l;
	init(l);
	add(l, 1);
	//add(l, 2);
	//add(l, 3);

	printf("%i",length(l));
}
