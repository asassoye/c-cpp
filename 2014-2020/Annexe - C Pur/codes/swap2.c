/*
 * =======================================================================
 * Name        : 12_Swap.c
 * Author      : nvs & smb
 * Description : des fonctions qui permutent le contenu de variables
 * =======================================================================
 */

#include <stdio.h>

void swapKO(int a, int b)
{
    int x = a;
    a = b;
    b = x;
}

void swapOK(int *a, int *b)
{
    int x = *a;
    *a = *b;
    *b = x;
}

typedef enum
{
    CHAR, INT, DOUBLE
} TYPE;

// pointeur générique
void swap(void *a, void *b, TYPE type)
{
    switch (type)
    {
        case CHAR:
        {
            char x = *(char *) a;
            *(char *) a = *(char *) b;
            *(char *) b = x;
        }
        break; // fin case CHAR

        case INT:
        {
            int x = *(int *) a;
            *(int *) a = *(int *) b;
            *(int *) b = x;
        }
        break; // fin case INT

        case DOUBLE:
        {
            double x = *(double *) a;
            *(double *) a = *(double *) b;
            *(double *) b = x;
        }
        break; // fin case DOUBLE
    } // fin switch
}

int main()
{
    int i = 8, j = -9;

    printf("a) i == %2d\t\tj == %2d\n", i, j);

    swapKO(i, j);

    printf("b) i == %2d\t\tj == %2d\n", i, j);

    swapOK(&i, &j);

    printf("c) i == %2d\t\tj == %2d\n", i, j);

    swap(&i, &j, INT);

    printf("d) i == %2d\t\tj == %2d\n\n", i, j);

    // --------------------------------------------------

    char c = 'c', d = 66;

    //swapOK(&c, &d);   // compile pas

    printf("e) c == '%c' (%d)\t\td == '%c' (%d)\n", c, c, d, d);

    swap(&c, &d, CHAR);

    printf("f) c == '%c' (%d)\t\td == '%c' (%d)\n", c, c, d, d);

    return 0;
}
