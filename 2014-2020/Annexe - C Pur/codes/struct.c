#include <stdio.h>

struct point
{
	int x;
	int y;
	int z;
};

struct point_p
{
	int x;
	int y;
};

void affiche(struct point p)
{
	//printf("( %i , %i)\n", p.x, p.y);
	printf("( %i , %i, %i)\n", p.x, p.y, p.z);
};

int main()
{
	struct point p;
	
	affiche(p);//why is y init to 0 ? rdm ? if z still 0, z rdm

	p.x = 1;
	p.y = 2;

	affiche(p);

	//struct point_p ppp;
	//ppp = p; //nope
	//ppp = (struct point)p; //still no
	struct point p2 = {2};
	affiche(p2);

	return 0;
}
