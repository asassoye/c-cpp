#include <stdio.h>

int main()
{
	int t1[5] = {1,2,3,4,5};//try with 8 and print to 8
	int t2[] = {1,2,3,4,5};
	int * t3 = t2;
	//int t4[]; //ERROR	

	for(int i = 0; i < 5; i++)
	{
		printf("%i\n",t1[i]);
		printf("%i\n",t2[i]);
		printf("%i\n",t3[i]);		
	}
	printf("\n");	

	int t5[5];
	for(int i = 0; i < 5; i++)
		printf("%i\n",t5[i]);
	printf("\n");

	int t6[3][4] = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};//try to remove 8
	for(int i = 0; i < 3; i++)
	{
		for(int j = 0; j< 4; j++)
			printf("%i ", t6[i][j]);
		printf("\n");
	}
}
