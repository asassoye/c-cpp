#include <stdio.h>

void swap(void* u, void* v)
{
	void* tmp = u;
	*u = *v;
	*v = *tmp;
}

int main()
{
	int i = 1;
	int j = 2;
	
	printf("%d\n",i);
	printf("%d\n",j);
	printf("\n");

	swap(&i, &j);
	printf("%d\n",i);
	printf("%d\n",j);
	printf("\n");
}
