#include <stdio.h>
#include <stdlib.h>

int main()
{
	int r = 4;
	int c = 5;

	int tab[r][c]; //automatic array

	for(int i = 0; i < r; i++)
		for(int j = 0; j < c; j++)
			tab[i][j] = i + j;
	
	for(int i = 0; i < r; i++)
	{		
		for(int j = 0; j < c; j++)
			printf("%2d",tab[i][j]);
		printf("\n");
	}

	printf("\n");

	int** tab2 = (int**) malloc(r * sizeof(int*)); //dynamic array
	for(int  i = 0; i < r; i++)
		tab2[i]  = malloc( c * sizeof(int));
	
	for(int i = 0; i < r; i++)
		for(int j = 0; j < c; j++)
			tab2[i][j] = i + j;

	for(int i = 0; i < r; i++)
	{		
		for(int j = 0; j < c; j++)
			printf("%2d",tab2[i][j]);
		printf("\n");
	}
	
	//frees the complete array
	for(int  i = 0; i < r; i++)
		free(tab2[i]);
	free(tab2); 

	int ** tab3 = (int**) malloc(r * c * sizeof(int));
	for(int i = 0; i < r; i++)
		for(int j = 0; j < c; j++)
			tab3[i][j] = i + j;

	for(int i = 0; i < r; i++)
	{		
		for(int j = 0; j < c; j++)
			printf("%2d",tab3[i][j]);
		printf("\n");
	}
}
