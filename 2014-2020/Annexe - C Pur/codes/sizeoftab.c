#include <stdio.h>


void f(char *t)
{
	printf("%i / %i\n", sizeof t, sizeof t[0]);
	printf("%i\n", sizeof t / sizeof t[0]);
}

/*
void f(char t[])
{
	printf("%i / %i\n", sizeof t, sizeof t[0]);
	printf("%i\n", sizeof t / sizeof t[0]);
}*/


int main()
{
	char t[5] = {'a','b','c','d','e'};
	printf("%i\n", sizeof t / sizeof t[0]);
	f(t);
}
