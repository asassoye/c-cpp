#include <stdio.h>

typedef enum
{
	FALSE=0, TRUE=1
} BOOL;

void crie(BOOL tab[], int size, void (*)(BOOL) );
void aboyer(BOOL);
void miauler(BOOL);

int main()
{
	BOOL cri[] = {TRUE,FALSE,TRUE,TRUE,FALSE};
	int n = sizeof cri / sizeof (*cri);

	crie(cri, n, aboyer);//pointeur de fct : nom sans param
	printf("\n");
	crie(cri, n, miauler);

	crie(cri, n, truc);
}

void crie(BOOL tab[], int size, void (*f)(BOOL) )
{
	for(int i = 0; i < size; i++)
		f(tab[i]);
}

void aboyer(BOOL b)
{
	if(b)
		printf("Wouf\n");
	else
		printf("...\n");
}

void miauler(BOOL b)
{
	if(b)
		printf("Miaou\n");
	else
		printf("...\n");
}
