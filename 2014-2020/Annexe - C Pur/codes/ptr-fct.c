#include <stdio.h>

inline int plus_one(int i)
{
	return i + 1;
}

//fct est une fonction qui prend en param un int et retourne un int
//                                            | 
void foreach(int* tab, int size, int (*fct)(int) )
{
	for(int i = 0; i < size; i++)
		tab[i] = fct(tab[i]);
}

int main()
{
	int tab[] = {1,2,3,4,5,6};
	int size = sizeof(tab) / sizeof(*tab);

	for(int i = 0; i < size; i++)
		printf("%d ", tab[i]);
	printf("\n");

	foreach(tab, size, plus_one);

	for(int i = 0; i < size; i++)
		printf("%d ", tab[i]);
	printf("\n");
}
