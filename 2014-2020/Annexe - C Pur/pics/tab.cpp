#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

int main()
{
	int m = 4;
	int n = 5;

	vector<vector<int> > v(m,vector<int>(n));
	
	for(int i = 0; i < m; i++)
		for(int j = 0; j < n; j++)
			v[i][j] = i * j + (j - i);

	for(int i = 0; i < m; i++)
	{
		for(int j = 0; j < n; j++)
			cout << setw(3) << v[i][j] << " ";
		cout << endl;
	}
}
