#include <iostream>

using namespace std;

struct Mere
{
	int i = 1;
	virtual void print () { cout << "M" << endl; }
	void truc() { cout << "MT" << endl; }
};

struct Fille : public Mere
{
	int j = 2;
	void print() { cout << "F" << endl; }

	void truc() { cout << "TF" << endl; }
};

int main()
{
	Fille f;

	cout << f.i << " " << f.j << endl;

	Mere& m = f;

	f.print(); //F
	m.print(); //F
	f.truc(); //TF
	m.truc(); //MT

	cout << m.i << endl; //peut pas accéder à j;

	Fille& f2 = dynamic_cast<Fille&>(m);
	
	cout << f2.i << " " << f2.j << endl; //ok
}
