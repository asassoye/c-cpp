#include <iostream>

using namespace std;

struct A
{
	int i;
	A(int i = 0) : i(i) 
	{
		cout << "+A " << i << endl;
	}
};

struct B1 : virtual A
{
	int j1;
	B1(int j = 0) : A(j), j1(j + 1)
	{
		cout << "+B1 " << j << " " << j1 <<endl;
	}
};

struct B2 : virtual A
{
	int j2;
	B2(int j = 0) : A(j), j2(j + 2)
	{
		cout << "+B2 " << j << " " << j2 <<endl;
	}
};

struct C : B1, B2
{
	C(int j1 = 0, int j2 = 0) : A((j1 + j2) / 2), B1(j1), B2(j2)
	{
		cout << "+C" << endl;
	}
};

int main()
{
	C c (1, 7);
	cout << endl;

	cout << c.i << endl;
	cout << c.B1::i << endl;
	cout << c.B2::i << endl;
	cout << c.j1 << endl;
	cout << c.j2 << endl;
}
