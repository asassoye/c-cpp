#include <iostream>

using namespace std;

struct Mere
{	
	static int id_count;
	int id;

	int brol;

	Mere(int brol = 0) :  id(id_count), brol(brol) { id_count++; }

	Mere(const Mere& m) : Mere(m.brol) { cout << "r "; } //incrémente id_count
	Mere& operator=(const Mere& m)
	{
		if(&m != this)
		{
			id = id_count;
			id_count++;

			brol = m.brol;
		}

		cout << "= ";
		
		return *this;
	}

	virtual void print() { cout << "Mère " << id << " " << brol << endl; }	
};

int Mere::id_count = 1;

struct Fille : Mere
{
	Fille(int brol = 0) : Mere(brol) {}

	void print() { cout << "Fille " << id << " " << brol << endl; }	
};

void print_indep(Mere m) { m.print(); }
void print_indep(Fille f) { f.print(); } //try to remove this

int main()
{
	Mere m(1); Fille f(2);
	m.print(); f.print();

	Mere & rm = m; Fille & rf = f;
	rm.print(); rf.print();

	cout << endl;
	print_indep(m);
	print_indep(f);
	print_indep(rm);
	print_indep(rf);
	cout << endl;

	rm = rf;
	rm.print(); rf.print();
	cout << endl;

	Mere & rm2 = f;
	rm2.print();
	print_indep(rm2);
}
