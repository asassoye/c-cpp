#include <iostream>

using namespace std;

struct Mere
{
	int i;

	Mere(int i) : i(i) {}

	virtual void print() { cout << "Mère " << i << endl; }	
};

struct Fille : Mere
{
	Fille(int i) : Mere(i) { this->i = i; }

	void print() { cout << "Fille " << i << endl; }	
};

void print_indep(Mere& m) { m.print(); }

void print_indep(Mere* m) { m->print(); }

int main()
{
	Mere m(1); Fille f(2);
	m.print(); f.print();
	cout << endl;

	Mere & rm = m; Fille & rf = f;
	rm.print(); rf.print();
	cout << endl;

	rm = rf; //changes i in rm
	rm.print(); rf.print();
	cout << endl;

	print_indep(m);
	print_indep(f);
	print_indep(rm);
	print_indep(rf);
	print_indep(&m);
	print_indep(&f);
	print_indep(&rm);
	print_indep(&rf);
	
	cout << endl;

	Mere & rm2 = rf;
	rm2.print();
	print_indep(rm2);

	Mere * ptm = &f;
	ptm->print();
	print_indep(ptm);	
}
