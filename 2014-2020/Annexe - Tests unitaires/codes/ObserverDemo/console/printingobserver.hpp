#ifndef PRINTINGOBSERVER_H
#define PRINTINGOBSERVER_H

#include "arrayevent.hpp"
#include "arrayobserver.hpp"

#include <iostream>

/**
 * This class models a simple concrete observer for arrays
 * printing in the standard output information regarding
 * the operations performed in the observed rray.
 * @brief This class models an concrete observer for arrays
 * printing information regarding performed operations.
 * @tparam T the type of the datas of the oberved array
 * @see ArraySubject<T>
 */
template<class T>
class PrintingObserver : public ArrayObserver<T>
{
    public:
       /**
         * Prints in the standard output that an element
         * <code>d</code> has been set in observed array
         * at a specified index <code>i</code>.
         * <p>
         * Information about the array, the element and its
         * position are extracted from the event parameter.
         * @brief Prints in the standard output that an element
         * has been set.
         * @param e the event associated with the set operation
         */
        void elementSet(ArrayEvent<T> e);

        /**
          * Prints in the standard output that an element
          * <code>d</code> has been obtained in observed array
          * at a specified index <code>i</code>.
          * <p>
          * Information about the array, the element and its
          * position are extracted from the event parameter.
          * @brief Prints in the standard output that an element
          * has been obtained.
          * @param e the event associated with the get operation
          */
        void elementGot(ArrayEvent<T> e);

        /**
          * Prints in the standard output that two elements
          * have been swapped in observed array.
          * <p>
          * Information about the array and the swapped elements
          * are extracted from the event parameter.
          * @brief Prints in the standard output that two elements
          * have been swapped.
          * @param e the event associated with the swap operation
          */
        void elementsSwapped(SwapEvent<T> e);

        /**
          * Prints in the standard output that the size of an
          * observed array has been requested.
          * <p>
          * Information about the array and the requested size
          * are extracted from the event parameter.
          * @brief Prints in the standard output that the size of an
          * observed array has been requested.
          * @param e the event associated with the size operation
          */
        void sizeRequested(SizeEvent<T> e);
};

template<class T>
void PrintingObserver<T>::elementGot(ArrayEvent<T> e)
{
    std::cout << "Element " << e.getData()
              << " at index " << e.getIndex()
              << " was obtained" << std::endl;
}

template<class T>
void PrintingObserver<T>::elementSet(ArrayEvent<T> e)
{
    std::cout << "Element at index " << e.getIndex()
              << " was set by " << e.getData() << std::endl;
}

template<class T>
void PrintingObserver<T>::elementsSwapped(SwapEvent<T> e)
{
    std::cout << "Elements at indexes " << e.getIndex1()
              << " and " << e.getIndex2()
              << " were swapped" << std::endl;
}

template<class T>
void PrintingObserver<T>::sizeRequested(SizeEvent<T> e)
{
    std::cout << "Size of " << e.getSize()
              << " of the array was requested" << std::endl;
}

#endif // PRINTINGOBSERVER_H
