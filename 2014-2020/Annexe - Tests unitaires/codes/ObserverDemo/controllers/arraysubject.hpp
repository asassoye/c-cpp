#ifndef ARRAYSUBJECT_H
#define ARRAYSUBJECT_H

#include "array.hpp"
#include "arrayevent.hpp"
#include "arrayobserver.hpp"

#include <list>
#include <iostream>

/**
 * This class models a standard array with basic operations
 * of setting, getting and swapping elements, which can be observed.
 * <p>
 * This means that every single time an operation is performed on the
 * underlying array, registered observers will be notified in order
 * to both fire the associated events as well as performing appropriate
 * actions.
 * <p>
 * The template parameter is the type of the datas stored in the array.
 * @brief This class simply models an array that can be observed.
 * @tparam T The type of the stored datas
 */
template<class T>
class ArraySubject : public Array<T>
{
    //memory referenced by pointers is not allocated here
    //the address is taken when adding the obs
    //no need to worry about freeing memory
    //no need to worry about copy
    //no need to worry about affectation
    //I know... I'm scared as fuck...
    std::list<ArrayObserver<T>* > observers;

    public:
        /**
         * Builds up an empty observable array with <code>n</code> elements.
         * <p>
         * Note that initially, the allocated memory is not set to any
         * kind of specific value. Hence, getting unset elements from
         * this allocated memory may result in unpredictable values.
         * @brief Builds up an empty array with <code>n</code> elements.
         * @param n the number of elements of the underlying array.
         * @throw std::invalid_argument if the size of the array is negative.
         */
        ArraySubject(int n);

        /**
         * Builds up an observable array with <code>n</code> elements copied from
         * a specified memory space.
         * <p>
         * Note that no particular bound control is made in this specifid
         * constructor. Hence, reading too many elements from a memoty space
         * too tiny may result in unpredictable results, such as random values
         * or segmentation faults.
         * @brief Builds up an observable array with <code>n</code> elements copied
         * from a specified memory space.
         * @param n the number of elements to copy from memory.
         * @param array the adress of the memory space to cpy elements
         * from.
         * @throw std::invalid_argument if the size of the array is negative.
         */
        ArraySubject(int n, T * t);

        /**
         * Builds up an observable array from an initialiser list.
         * @brief Builds up an observable array from an initialiser list.
         * @param list the initialiser list provided by instanciation.
         */
        ArraySubject(std::initializer_list<T> list);

        /**
         * Copies an array and makes it a subject.
         * @brief Copies an array and makes it a subject.
         * @param a the array to copy
         */
        ArraySubject(const Array<T>& a);

        /**
         * Registers an observer to the underlying observable array.
         * @brief Registers an observer to the underlying observable array.
         * @param obs the observer to register
         */
        inline void registerObserver(ArrayObserver<T>& obs);

        /**
         * This operator simply returns the element indexed at the specified
         * position in the underlying array, fires an ArrayEvent and notifies
         * registered observers.
         * <p>
         * The fired event is made using the underlying array as the source of the
         * event, the returned element as data and the position of that element
         * as index.
         * @brief This operator returns the element indexed at the specified
         * position in the underlying array and notifies registered observers.
         * @param i the index of the element to get
         * @return the element indexed at the specified position in the underlying
         * array.
         * @see ArrayEvent
         */
        inline virtual const T& operator[] (int i) const;

        /**
         * Sets the element indexed at the specified index to the given value,
         * fires an ArrayEvent and notifies registered observers.
         * <p>
         * The fired event is made using the underlying array as the source of the
         * event, the set element as data and the position of that element
         * as index.
         * @brief Sets the element indexed at the specified index to the given
         * value and notifies registered observers.
         * @param i the index of the element to set
         * @param data the element to set
         * @see ArrayEvent
         */
        virtual void set(int i, T data);

        /**
         * Swaps two elements indexed at the specified positions, fires a SwapEvent
         * and notifies registered observers.
         * <p>
         * The fired event is made using the underlying array as the source of the
         * event, and the two positions of the swapped elements as indexes.
         * @brief Swaps two elements indexed at the specified positions and notifies
         * registered observers.
         * @param i the index of the first element to swap
         * @param j the index of the second element to swap
         * @see SwapEvent
         */
        virtual void swap(int i, int j);

        /**
         * Returns the size of the underlying array, fires a SizeEvent and notifies
         * registered observers.
         * <p>
         * The fired event is made using the underlying array as the source of the
         * event, and the two positions of the swapped elements as indexes.
         * @brief Returns the size of the underlying array and notifies registered
         * observers.
         * @return the size of the underlying array.
         */
        inline int size() const;
};

template<class T>
ArraySubject<T>::ArraySubject(int n) : Array<T>(n)
{
    observers = std::list<ArrayObserver<T>* >();
}

template<class T>
ArraySubject<T>::ArraySubject(int n, T *t) : Array<T>(n, t)
{
    observers = std::list<ArrayObserver<T>* >();
}

template<class T>
ArraySubject<T>::ArraySubject(std::initializer_list<T> list)
    : Array<T>(list)
{
    observers = std::list<ArrayObserver<T>* >();
}

template<class T>
ArraySubject<T>::ArraySubject(const Array<T> &a) : Array<T>(a.size())
{
    for(int i = 0; i < a.size(); i++)
        Array<T>::set(i, a[i]);

    observers = std::list<ArrayObserver<T>* >();
}

template<class T>
void ArraySubject<T>::registerObserver(ArrayObserver<T>& obs)
{
    observers.push_back(&obs);
}

template<class T>
const T& ArraySubject<T>::operator [](int i) const
{
    const T& data = Array<T>::operator [](i);

    ArrayEvent<T> e(*this, data, i);
    for(ArrayObserver<T>* obs : observers)
        obs->elementGot(e);

    return data;
}

template<class T>
void ArraySubject<T>::set(int i, T data)
{
    Array<T>::set(i, data);

    ArrayEvent<T> e(*this, data, i);
    for(ArrayObserver<T>* obs : observers)
        obs->elementSet(e);
}

template<class T>
void ArraySubject<T>::swap(int i, int j)
{
    Array<T>::swap(i, j);

    SwapEvent<T> e(*this, i, j);
    for(ArrayObserver<T>* obs : observers)
        obs->elementsSwapped(e);
}

template<class T>
int ArraySubject<T>::size() const
{
    int s = Array<T>::size();
    SizeEvent<T> e(*this, s);

    for(ArrayObserver<T>* obs : observers)
        obs->sizeRequested(e);

    return s;
}

#endif // ARRAYSUBJECT_H
