#ifndef ARRAYEVENT_H
#define ARRAYEVENT_H

#include "array.hpp"

/**
 * This base class models simple array events.
 * <p>
 * The only information stored within the event is
 * the array which responsible for firing the event.
 * <p>
 * As a consequence, the user is prevented from direcly
 * instanciating objects from this class, since constructors
 * are protected. Instead, he can derivate this class and
 * instanciated subclasses which are relevent to what happened
 * to the observed array.
 * @brief This base class models simple array events.
 * @tparam T the type of the datas stored in the observed array.
 */
template<class T>
class BasicArrayEvent
{
    protected:
        /**
         * The array responsible from firing the event.
         * @brief The array responsible from firing the event
         */
        const Array<T>& source;

        /**
         * Builds up a simple array event with the source array
         * firing it.
         * @brief Builds up a simple array event with the source array
         * firing it
         * @param source the array responsible from firing the event
         */
        BasicArrayEvent(const Array<T>& source);

    public:
        /**
         * Returns the array that fired the underlying event.
         * @brief Returns the array that fired the underlying event.
         * @return the array that fired the underlying event
         */
        inline const Array<T>& getSource() const;
};

/**
 * This class models array events affecting only a single data
 * withing the array, such as the operations of getting or setting
 * an element withing an array.
 * <p>
 * This event stores information regarding the array responsible from
 * firing it, along with the affected data, and the index of the affected
 * data.
 * <p>
 * In the case of a get operation, the stored data is the returned
 * element, and in the case of a set event it is the new value of the
 * set element.
 * @brief This class models events fired by get and set operations.
 * @tparam T the type of the datas stored in the observed array.
 */
template<class T>
class ArrayEvent : public BasicArrayEvent<T>
{
    T data;
    int i;

    public:
        /**
         * Builds up an event with the source array firing it, the
         * affected data and the index of the affected data.
         * <p>
         * In the case of a get operation, the stored data is the returned
         * element, and in the case of a set event it is the new value of the
         * set element.
         * @brief Builds up an event fired by get and set operations.
         * @param source the array responsible from firing the event
         * @param data the data affected by the operation firing the event
         * @param i the index of the affected data
         */
        ArrayEvent(const Array<T>& source, T data, int i);        

        /**
         * Returns the data affected by the operation firing the event.
         * @brief Returns the data affected by the operation firing the event.
         * @return the data affected by the operation firing the event.
         */
        inline T getData() const;

        /**
         * Returns the index of the data affected by the operation firing the
         * event.
         * @brief Returns the index of the data affected by the operation
         * firing the event.
         * @return the index of the data affected by the operation firing
         * the event.
         */
        inline int getIndex() const;
};

/**
 * This class models array events fired by swap operations.
 * <p>
 * This event stores information regarding the array responsible from
 * firing it, along with the indexes of the affected datas.
 * @brief This class models events fired by swap operations.
 * @tparam T the type of the datas stored in the observed array.
 */
template<class T>
class SwapEvent : public BasicArrayEvent<T>
{
    int i, j;

    public:
        /**
         * Builds up an event fired by swap opertions with the source
         * array firing it, and the indexes of the affected datas.
         * @brief Builds up an event fired by swap operations.
         * @param source the array responsible from firing the event
         * @param i the index of the first affected data
         * @param j the index of the second affected data
         */
        SwapEvent(const Array<T> &source, int i, int j);

        /**
         * Returns the index of the first data affected by the operation
         * firing the event.
         * @brief Returns the index of the first data affected by the
         * operation firing the event.
         * @return the index of the first data affected by the operation
         * firing the event.
         */
        inline int getIndex1() const;

        /**
         * Returns the index of the second data affected by the operation
         * firing the event.
         * @brief Returns the index of the second data affected by the
         * operation firing the event.
         * @return the index of the second data affected by the operation
         * firing the event.
         */
        inline int getIndex2() const;
};

/**
 * This class models events fired by size requiring operations.
 * <p>
 * This event stores information regarding the array responsible from
 * firing it, along with the the value of the requested size.
 * @brief This class models events fired by size requiring operations.
 * @tparam T the type of the datas stored in the observed array.
 */
template<class T>
class SizeEvent : public BasicArrayEvent<T>
{
    int size;

    public:
        /**
         * Builds up an event fired by size requesting opertions with
         * the source array firing it, and value of the requested size.
         * @brief Builds up an event fired by size requesting operations.
         * @param source source the array responsible from firing the event
         * @param size the requested size
         */
        SizeEvent(const Array<T>& source, int size);

        /**
         * Returns the size of the observed array.
         * @brief Returns the size of the observed array.
         * @return the size of the observed array
         */
        inline int getSize() const;
};

template<class T>
BasicArrayEvent<T>::BasicArrayEvent(const Array<T> &source) : source(source) {}

template<class T>
const Array<T>& BasicArrayEvent<T>::getSource() const
{
    return source;
}

template<class T>
ArrayEvent<T>::ArrayEvent(const Array<T> &source, T data, int i)
    : BasicArrayEvent<T>(source), data(data), i(i) {}

template<class T>
T ArrayEvent<T>::getData() const
{
    return data;
}

template<class T>
int ArrayEvent<T>::getIndex() const
{
    return i;
}

template<class T>
SwapEvent<T>::SwapEvent(const Array<T> &source, int i, int j)
    : BasicArrayEvent<T>(source), i(i), j(j) {}

template<class T>
int SwapEvent<T>::getIndex1() const
{
    return i;
}

template<class T>
int SwapEvent<T>::getIndex2() const
{
    return j;
}

template<class T>
SizeEvent<T>::SizeEvent(const Array<T> &source, int size)
    : BasicArrayEvent<T>(source), size(size) {}

template<class T>
int SizeEvent<T>::getSize() const
{
    return size;
}
#endif // ARRAYEVENT_H
