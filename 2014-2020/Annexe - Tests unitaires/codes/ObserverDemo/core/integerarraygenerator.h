#ifndef INTEGERARRAYGENERATOR_H
#define INTEGERARRAYGENERATOR_H

#include "array.hpp"

class IntegerArrayGenerator
{
    int min, max;

    public:
        IntegerArrayGenerator(int min, int max);
        Array<int>* ascending();
        Array<int>* descending();
        Array<int>* random();
};

#endif // INTEGERARRAYGENERATOR_H
