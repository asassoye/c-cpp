#ifndef HEAPSORT_H
#define HEAPSORT_H

#include "array.hpp"
#include "sortalgorithm.hpp"

template<class T>
class HeapSort : public SortAlgorithm<T>
{
    private:
        int heapsize;        

    public:
        HeapSort(Array<T>& a);
        void sort();        

    private:
        void buildHeap();
        void heapify(int i);
};

template<class T>
HeapSort<T>::HeapSort(Array<T> &array) : SortAlgorithm<T>(array), heapsize(array.size()) {}

template<class T>
void HeapSort<T>::sort()
{
    buildHeap();

    for (int i = this->a.size() - 1; i >= 1; i--)
    {
        this->a.swap(0, i);
        heapsize--;
        heapify(0);
    }
}

template<class T>
void HeapSort<T>::buildHeap()
{
    for (int i = heapsize / 2; i >= 0; i--)
        heapify(i);
}

template<class T>
void HeapSort<T>::heapify(int i)
{
    bool done = false;
    while(!done)
    {
        int indexleft = 2 * i + 1;
        int indexright = 2 * i + 2;
        int maxindex;

        if(indexleft < heapsize && this->a[indexleft] > this->a[i])
            maxindex = indexleft;
        else
            maxindex = i;

        if(indexright < heapsize && this->a[indexright] > this->a[maxindex])
            maxindex = indexright;

        if(i != maxindex)
        {
           this->a.swap(i, maxindex);
           i = maxindex;
        }
        else
            done = true;
    }   
}

#endif // HEAPSORT_H
