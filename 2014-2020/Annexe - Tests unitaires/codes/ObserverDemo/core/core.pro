include(../defaults.pri)

TEMPLATE = lib
TARGET = libcore
DESTDIR = ../lib

HEADERS += \
    array.hpp \
    bubblesort.hpp \
    heapsort.hpp \
    insertionsort.hpp \
    integerarraygenerator.h \
    permutationsort.hpp \
    sortalgorithm.hpp

SOURCES += \
    integerarraygenerator.cpp
