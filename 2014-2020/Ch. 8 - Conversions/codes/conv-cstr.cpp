#include <iostream>

using namespace std;

class A
{
	int i;
	
	public:
		A(int i) : i(i) 
		{
			cout << "Cstr " << i << endl;
		}

		A(const A& a) : i(a.i) 
		{
			cout << "Cstr-c " << a << endl;
		}

		//friend ostream& operator << (ostream& out, const A& a);
};

void f(A a) 
{
	cout << "f with " << a << endl;
}

ostream& operator << (ostream& out, const A& a)
{
	out << a.i;
	return out;
}

int main()
{
	A a(2);

	cout << a << endl;

	a = 3;

	cout << a << endl;

	f(a);
	f(4);
}
