#include <iostream>

using namespace std;

struct Duck
{
	void quack() const { cout << "Quack" << endl; }
};

struct Pig
{
	void groink() const { cout << "Groink" << endl; }
};

struct CPPTeacher
{
	void quack() const { cout << "Quack quack" << endl; }
	void groink() const { cout << "Groink groink" << endl; }
};
