#include <iostream>
#include <type_traits>

#include "duck.hpp"

using namespace std;

//////////////////////////////////////////////////////////////////

template<class T>
constexpr auto test_duck(const T& t, int)
	-> decltype(t.quack(), bool())
{
    return true;
}

template<class T>
constexpr auto test_duck(const T& t, long)
{
    return false;
}

template<class T>
constexpr bool is_duck(const T& t)
{
    return test_duck(t, 0);
}

//////////////////////////////////////////////////////////////////

template<class T>
void make_noise(const T& t)
{
	if constexpr(is_duck(t))
		t.quack();
	else
		t.groink();
}

///////////////////////////////////////////////////////////////////

int main()
{	
	Duck duck;
	Pig pig;
	CPPTeacher abs;

	make_noise(duck);
	make_noise(pig);
	make_noise(abs);
}
