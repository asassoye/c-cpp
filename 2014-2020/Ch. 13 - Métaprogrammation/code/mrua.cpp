#include <iostream>

using namespace std;

class MRUA
{
	long double x0;
	long double t0;
	long double v0;
	long double a;

	public:
		MRUA(long double x0 = 0, long double t0 = 0, long double v0 = 0, long double a = 9.81)
			: x0(x0), t0(t0), v0(v0), a(a)
		{}

		long double operator()(long double t)
		{
			return x0 + v0 * (t - t0) + 0.5 * a * (t - t0) * (t - t0);
		}
};

long double operator "" _ms(long double x) { return 0.001 * x; }
long double operator "" _s(long double x) { return x; }
long double operator "" _min(long double x) { return x * 60; }
long double operator "" _h(long double x) { return x * 3600; }

int main()
{
	cout << "1ms is " << 1.0_ms << " s" << endl;

	MRUA mrua;
	cout << "Position after 1 millisecond is " << mrua(1.0_ms) << " meters" << endl;
	cout << "Position after 1 second is " << mrua(1.0_s) << " meters" << endl;
	cout << "Position after 1 minute is " << mrua(1.0_min) << " meters" << endl;	
	cout << "Position after 1 hour is " << mrua(1.0_h) << " meters" << endl;		
}
