#include <iostream>

using namespace std;

template<class T>
class A
{
	T t;
	int i;

	public:
		A(int i) : i(i) {}
		A(T t) : t(t), i(-1) {}
};

int main()
{
	A<double> a; //ok
	A<int> b; //ko
}
