#include <iostream>
#include <type_traits>

#include "duck.hpp"

using namespace std;

/////////////////////////////////////////////////////////////

template<class>
struct sfinae_true : true_type {};

template<class T>
static constexpr auto test_duck(int) 
	-> sfinae_true<decltype(declval<T>().quack())>;

template<class>
static constexpr auto test_duck(long)
	-> false_type;

template<class T>
struct bool_duck : decltype(test_duck<T>(0)){};

//////////////////////////////////////////////////////////////

template<class T>
using enable_for_duck = enable_if_t<bool_duck<T>::value>;

template<class T>
using disable_for_duck = enable_if_t<! bool_duck<T>::value>;

template<class T, enable_for_duck<T>* = nullptr>
void make_noise(const T& t)
{
	t.quack();
}

template<class T, disable_for_duck<T>* = nullptr>
void make_noise(const T& t)
{
	t.groink();
}

int main()
{
	Duck duck;
	Pig pig;
	CPPTeacher abs;

	make_noise(duck);
	make_noise(pig);
	make_noise(abs);
}
