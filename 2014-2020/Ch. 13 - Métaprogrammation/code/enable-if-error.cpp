#include <iostream>

using namespace std;

template<class Integer>
using isAnInt = std::enable_if_t<std::is_integral<Integer>::value>;

template<class Float>
using isAFloat = std::enable_if_t<std::is_floating_point<Float>::value>;

struct Wrapper
{	
    template <typename Integer, class = isAnInt<Integer>>
    Wrapper(Integer) { cout << "int" << endl; }
 
    template <typename Floating, class = isAFloat<Floating>>
    Wrapper(Floating) { cout << "float" << endl; } //cannot overload
};

int main()
{
	Wrapper(1);
	Wrapper(1.0);
}
