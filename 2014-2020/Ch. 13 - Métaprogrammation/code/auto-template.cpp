#include <iostream>

using namespace std;

struct A
{
	A a() {}
};

struct B
{
	B b() {}
};

struct AB
{
	AB a() {}
	AB b() {}
};

template<class T>
class Wrapper
{
	T& t;
	
	public:
		Wrapper(T& t) : t(t) {}
		auto a() { return t.a(); }
		auto b() { return t.b(); }
};

int main()
{
	A a; B b; AB ab;
	Wrapper<A> wa(a); wa.a(); //wa.b();
	Wrapper<B> wb(b); /*wb.b();*/ wb.b();
	Wrapper<AB> wab(ab); wab.a(); wab.b();
}
