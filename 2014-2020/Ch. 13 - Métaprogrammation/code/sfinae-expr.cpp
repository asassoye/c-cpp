#include <iostream>

#include "duck.hpp"

template<class T>
constexpr auto test_duck(const T& t, int)
	-> decltype(t.quack(), bool())
{
    return true;
}

template<class T>
constexpr auto test_duck(const T& t, long)
{
    return false;
}

template<class T>
constexpr bool is_duck(const T& t)
{
    return test_duck(t, 0);
}

int main()
{
	Duck duck;
	Pig pig;
	CPPTeacher abs;

	cout << is_duck(duck) << endl;	
	cout << is_duck(pig) << endl;
	cout << is_duck(abs) << endl;	
}
