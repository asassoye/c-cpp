#include <iostream>
#include <cmath>

using namespace std;

template<class T = double> constexpr T PI = T(atan(1) * 4);

template<class T, class Tag> //tag is not used (phantom)
class NamedType
{ 
	T _val;	

	public:
		NamedType(T x = T()) : _val(x) {}
		T& val() { return _val; }
};

//struct RadiusTag {};
//using Radius = NamedType<double, RadiusTag>;
using Radius = NamedType<double, struct RadiusTag>; //same

//struct HeightTag {};
//using Diameter = NamedType<double, HeightTag>;
using Diameter = NamedType<double, struct DiameterTag>; //same

class Circle 
{
	double radius;

	public:
    	explicit Circle(Radius r) : radius(r.val()) {}
	    explicit Circle(Diameter d) : radius(d.val() / 2) {}	    
	    double area() { return PI<double> * radius * radius; }
};

int main()
{
	Circle c1(Radius(7));
	Circle c2(Diameter(7));
}
