#include <iostream>

using namespace std;

template<class T>
class A
{
	T t;
	int i;

	public:
		A(int i) : i(i) {}
		A(T t) : t(t), i(-1) {}
};

template<>
class A<int>
{
	int i;

	public:
		A(int i) :  i(i) {} 
};

int main()
{
	A<double> a(1); //ok
	A<int> b(1); //ok
}
