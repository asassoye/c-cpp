#include <iostream>

using namespace std;

struct Duck
{
	Duck(int i) {} //no default constructor
	void quack() const {}
};

template<class>
struct sfinae_true : true_type{};

template<class T>
static auto test_duck(int) 
	-> sfinae_true<decltype(T().quack())>; //no declval

template<class>
static auto test_duck(long)
	-> false_type;

template<class T>
struct bool_duck : decltype(test_duck<T>(0)){};

int main()
{
	cout << (bool_duck<Duck>() == true) << endl;
}
