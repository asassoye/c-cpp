#include <iostream>
#include <type_traits>

using namespace std;

template<class T>
using isNotInt = std::enable_if_t<! std::is_same_v<T,int>>;

template<class T>
class A
{
	T t;
	int i;

	public:
		A(int i) : i(i) {}

		template<class U = T, class = isNotInt<U>>
		A(T t) : t(t), i(-1) {}
};

int main()
{
	A<double> a(1); //ok
	A<int> b(1); //ok
}
