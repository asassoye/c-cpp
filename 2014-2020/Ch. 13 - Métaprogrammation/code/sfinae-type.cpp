#include <iostream>
#include <type_traits>

#include "duck.hpp"

using namespace std;

template<class>
struct sfinae_true : true_type{};

template<class T>
static auto test_duck(int) 
	-> sfinae_true<decltype(declval<T>().quack())>;

template<class>
static auto test_duck(long)
	-> false_type;

template<class T>
struct bool_duck : decltype(test_duck<T>(0)){};

int main()
{
	cout << (bool_duck<Duck>() == true) << endl;
	cout << (bool_duck<Pig>() == true) << endl;
	cout << (bool_duck<CPPTeacher>() == true) << endl;
}
