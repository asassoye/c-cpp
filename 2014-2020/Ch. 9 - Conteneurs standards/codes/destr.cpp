#include <iostream>
#include <vector>

using namespace std;

struct A
{
	A() { cout << "+A" << endl; }
	A(const A& a) { cout << "cA" << endl; }	
	~A() { cout << "-A"  << endl; }
};

int main()
{
	vector<A> v(5);
	cout << endl;
	vector<A> w(v);
	cout << endl;

	vector<A*> dyn;
	for(int i = 0; i < 5; i++)
		dyn.push_back(new A());	

	for(A * a : dyn)
	{
		delete a;
		a = nullptr;
	}

	cout << endl;
}
