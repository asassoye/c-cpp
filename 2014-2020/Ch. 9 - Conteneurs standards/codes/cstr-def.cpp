#include <iostream>
#include <vector>

using namespace std;

class point
{
	int x, y;

	public:
		point(int x, int y) : x(x), y(y) {}
	
		friend ostream& operator <<(ostream& out, const point& p);
};

ostream& operator<<(ostream& out, const point& p)
{
	out << "( " << p.x << " , " << p.y << " )";
	return out;
}

int main()
{
	vector<point> v(3);

	for(auto p : v)
		cout << p << endl;
}
