#include <iostream>
#include <vector>
#include <list>
#include <deque>

using namespace std;

int main()
{
	vector<int> v = {1, 2, 3, 4};
	list<int> l = {5, 6, 7, 8};
	deque<int> d = {10, 11, 12, 13};

	//direct random acces in constant time
	for(int i = 0; i < 4; i++)
		cout << v[i] << " " << d[i] << endl;
	cout << endl;

	cout << l.front() << endl;	
	cout << l.back() << endl;

	auto it = l.begin();
	it++;
	cout << *it << endl;
	it++;
	cout << *it << endl;
}
