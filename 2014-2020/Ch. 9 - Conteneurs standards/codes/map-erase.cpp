#include <iostream>
#include <map>

using namespace std;

int main()
{
	map<int, float> m;

	for(int i = 0; i <=10; i++)
		m[i] = i + 0.5;

	m.insert(std::make_pair(42,3.14));

	for(auto e : m)
		cout << "( " << e.first << " , " << e.second << " )" << endl;
	cout << endl;
	
	auto it1 = m.begin();
	auto it2 = m.begin();
	it1++;
	it2++; it2++; it2++; it2++;

	m.erase(it1, it2);

	for(auto e : m)
		cout << "( " << e.first << " , " << e.second << " )" << endl;
	cout << endl;
}
