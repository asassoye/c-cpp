#include <iostream>
#include <vector>

using namespace std;

int main()
{
	vector<int> v = {1, 2, 3, 4, 5};

	for(int& i : v)
		i+=2;

	for(int i : v)
		cout << i << " ";
	cout << endl;

	auto it = v.begin();
	while(it != v.end())
	{
		*it = *it * 2;
		it++;
	}

	for(int i : v)
		cout << i << " ";
	cout << endl;
}
