#include <iostream>
#include <stdexcept>
#include <deque>

using namespace std;

int main()
{
	deque<int> d = {1, 2, 3, 4, 5, 6};

	try
	{
		for(int i = 0; i < 100; i++)
			cout << d.at(i) << " ";			
	}
	catch(out_of_range& ex)
	{
		cout << "I'm out of range" << endl;
	}
	cout << endl;

	d.shrink_to_fit();
}
