#include <list>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{	
	list<int> l1 = {1, 6, 3, 9, 11, 18, 5};
	list<int> l2 = {12, 4, 9, 8};

	l1.sort(); l2.sort();
	l1.merge(l2);
	cout << "l1 = ";
	for_each(l1.begin(), l1.end(), [](int i) { cout << i << " "; }); cout << endl;
	cout << "l2 = ";
	for_each(l2.begin(), l2.end(), [](int i) { cout << i << " "; }); cout << endl;
	
	char c1[] = {"xyz"};
	char c2[] = {"abcdef"};
	list<char> l3 (c1, c1 + 3);
	list<char> l4 (c2, c2 + 6);
	
	auto it1 = l3.begin();
	auto it2 = l4.begin();
	it1++;it2++;it2++;

	l3.splice(it1, l3, it2, l4.end());	
	cout << "l3 = ";
	for_each(l3.begin(), l3.end(), [](char c) { cout << c << " "; }); cout << endl;
	cout << "l4 = ";
	for_each(l4.begin(), l4.end(), [](char c) { cout << c << " "; }); cout << endl;

	list<int> l5 = {1, 2, 3, 4, 5};
	cout << "front = " << l5.front() << " , back = " << l5.back() << endl;
	for_each(l5.begin(), l5.end(), [](int i) { cout << i << " "; }); cout << endl;
	l5.reverse();
	cout << "front = " << l5.front() << " , back = " << l5.back() << endl;
	for_each(l5.begin(), l5.end(), [](int i) { cout << i << " "; }); cout << endl;
}
