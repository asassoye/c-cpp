#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main ()
{
  vector<int> first, second, third;
  vector<float> fourth;
  vector<int> v1 {1,2,3,6};
  vector<int> v2 {2,3,4,5,0};

  cout << (v1 < v2) << endl;
  cout << (v2 < v1) << endl;

  v1.assign (7,100);     
  for_each(v1.begin(), v1.end(), [](int& i) { cout << i << " "; });
  cout << endl;    

  first.assign(7,50);  
  for_each(first.begin(), first.end(), [](int& i) { cout << i << " "; });
  cout << endl;    

  second.assign (first.begin()+1,first.end()-1);   
  for_each(second.begin(), second.end(), [](int& i) { cout << i << " "; });
  cout << endl;    

  int myints[] = {1776,7,4};
  third.assign (myints,myints+3);  
  for_each(third.begin(), third.end(), [](int& i) { cout << i << " "; });
  cout << endl;    

  fourth.assign(first.begin(), first.end()); //conversions
  for_each(fourth.begin(), fourth.end(), [](float& i) { cout << i << " "; });
  cout << endl;    

  cout << "Size of first : " << first.size() << endl;
  cout << "Size of second : " << second.size() << endl;
  cout << "Size of third : " << third.size() << endl;
  cout << "Size of fourth : " << fourth.size() << endl;
}
