#include <iostream>
#include <vector>

using namespace std;

template< class OutputIterator, class Function >
void apply(OutputIterator begin, OutputIterator end, Function f)
{
	while(begin != end)
	{
		f(*begin);
		begin++;
	}
}

void print(int n)
{
	cout << n << endl;
}

int main()
{
	vector<int> v = {1,2,3,4,5};
	apply(v.begin(), v.end(), print);	
}
