#include <iostream>
#include <array>

using namespace std;

class Brol
{
	const int _id;

	public:
		Brol(int id) : _id(id) {}
		int id() const { return _id; }		
};

int main()
{
	array<int,5> a;
	for(int i = 0; i < a.size(); i++)
		a[i] = i;

	for(int i : a)
		cout << i << " ";
	cout << endl;

	array<int,5> b = {0,1,2,3,4}; //try with one more and one less elem
	for(int i : b)
		cout << i << " ";
	cout << endl;

	//talk about 5 constexpr
}
