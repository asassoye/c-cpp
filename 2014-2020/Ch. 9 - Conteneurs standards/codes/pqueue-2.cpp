#include <deque>
#include <queue>
#include <iostream>
#include <stdlib.h>

using namespace std;

class Comparator
{
	public:
		bool operator () (int i, int j)//tells when is is placed after j
		{
			return i > j;
		}
};

int main()
{
	priority_queue<int, deque<int>, Comparator > q;
	
	for(int i = 0; i < 10; i++)
	{
		int r = rand() % 100 + 1;
		q.push(r);
		cout << "pushed " << r << endl;
	}	
	cout << endl;

	while(! q.empty())
	{
		cout << q.top() << " ";
		q.pop();
	}

	cout << endl;
}
