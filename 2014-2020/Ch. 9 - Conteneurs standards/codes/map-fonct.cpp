#include <iostream>
#include <map>

using namespace std;

struct mon_ordre
{
	bool operator() (int n, int p)
	{
		return n > p;
	}
};

int main()
{
	map<int, float, mon_ordre> m1;
	map<int, float> m2;

	for(int i = 0; i < 3; i++)
	{
		m1[i] = 0.5;
		m2[i] = 0.5;
	}

	for(auto e : m1)
		cout << "( " << e.first << " , " << e.second << " )" << endl;
	cout << endl;
	for(auto e : m2)
		cout << "( " << e.first << " , " << e.second << " )" << endl;

	cout << endl;	

	m1.key_comp()(1,2) ? cout << "1 plus petit que 2" : cout << "1 plus grand que 2";//olé
	cout << endl;

	auto i1 = m1.begin();	
	auto i2 = m1.begin();
	i2++;

	cout << m1.key_comp()((*i1).first, (*i2).first) << endl;
}
