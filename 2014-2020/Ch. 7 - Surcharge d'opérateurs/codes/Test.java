import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Test
{
	public static void main(String[] args)
	{		

		int i = 0;
		A a = new A();
		JButton b = new JButton();

		b.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.out.println(i);
				a.print();
			}
		});
	}
}

class A
		{
			public void print()
			{
				System.out.println("A");
			}
		}
