#include <iostream>

using namespace std;

unsigned gcd(unsigned a, unsigned b)
{
	if(b == 0)
		return a;
	else
		return gcd(b, a % b);
}

unsigned lcm(unsigned a, unsigned b)
{
	//return (a * b) / gcd(a, b); //BAD
	return (a / gcd(a,b)) * b; //GOOD
}

int main()
{

}
