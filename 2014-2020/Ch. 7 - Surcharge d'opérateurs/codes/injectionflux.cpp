#include <iostream>

using namespace std;

class A
{
	int i;

	public:
		inline A(int i = 0) : i(i)
		{}

		friend ostream& operator << (ostream& out, const A& a);
};

ostream& operator << (ostream& out, const A& a)
{
	out << "A : " << a.i;
	return out;
}

int main()
{
	A a1(2);
	A a2(3);

	cout << a1 << ", " << a2 << endl;
}
