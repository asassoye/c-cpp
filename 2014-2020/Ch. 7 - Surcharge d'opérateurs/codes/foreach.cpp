#include <iostream>
#include <vector>
#include <functional>

using namespace std;

//attention : container = qqch sur lequel je peux itérer qui contient des entiers
//f : qqch qui surcharge ()(int)
template<class Container, class Function>
void foreach(Container c, Function f) //with templates
{
	for(int i : c)
		f(i);
}

template<class Container>
void foreach2(Container c, function<void(int)> f)//with std::function wrapper
{
	for(int i : c)
		f(i);
}

void print(int i)
{
	cout << i << endl;
}

struct FoncteurPrint
{
	void operator() (int i)
	{
		cout << i << endl;
	}
};

int main()
{
	vector<int> v = {1, 2, 3, 4, 5};

	foreach(v, print);
	cout << endl;
	foreach(v, FoncteurPrint() );

	foreach(v, [&v](int i) { cout << &v << " : " << i << endl; } );        

	foreach2(v, print);
	cout << endl;
	foreach2(v, FoncteurPrint() );

	foreach2(v, [&v](int i) { cout << &v << " : " << i << endl; } );       
}
