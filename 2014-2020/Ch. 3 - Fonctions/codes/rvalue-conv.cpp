#include <iostream>

using namespace std;

int main()
{
	int a[] = {1, 2}; //C-style array : avoid
	int* pt = &a[0]; //adress of the first elem
	*(p + 1) = 10;   //OK : p + 1 is an rvalue, but *(p + 1) is an lvalue

	int i = 10;
	//int* pti = &(var + 1); // KO : lvalue required
	int* pti = &i;           // OK: i is an lvalue
	//&i = 20;               // KO : lvalue required

	//std::string& sref = std::string(); //KO : non const-ref init from rvalue
}
