#include <iostream>

using namespace std;

void f(int& i) {}

int main()
{
	int i = 2; int j = 2;

	int* const ptic  = &i;
	*ptic = 4;
	//cout << *ptic << endl;

	//ptic = &j;

	const int ci = 3; const int cj = 2;
	const int* cpti = &ci;

	//*cpti = 4;
	cpti = &cj;

	const int* const cptic = &ci; const int* const cptjc = &cj;

	//*cptic = 4;
	//cptic = cptjc;

	//f(2);

	int k = 4;
	int & rk = k;
	int* ptk = &rk;

	cout << k << endl;
	cout << rk << endl;
	cout << &k << endl;
	cout << *ptk << endl;
	cout << ptk << endl;
	cout << &ptk << endl;
}
