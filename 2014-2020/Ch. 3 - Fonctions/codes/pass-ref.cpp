#include <iostream>

using namespace std;

void countDown(int& i)
{
	while(i > 0)
	{
		cout << i << endl;
		i--;
	}
	cout << "BOOM" << endl;
}

int main()
{
	for(int i = 5; i >= 0; i--)
	{
		countDown(i);
		cout << endl;
	}
}
