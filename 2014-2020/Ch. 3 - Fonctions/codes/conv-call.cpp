#include <iostream>
#include <cmath>

using namespace std;

double divide_float(double i, double j)
{
	return i / j;
}

double divide_integer(int i, int j)
{
	return i / j;
}

int main()
{
	double PI = atan(1) * 4;
	double PI2 = PI * PI;

	double f = divide_float(PI2, PI);//no conversion
	cout << f << endl;
	f = divide_float(5,2);//5 and 2 converted
	cout << f << endl;
	int i = divide_float(PI2,PI);//return truncated
	cout << i << endl;
	i = divide_float(5,2);//5 and 2 converted and return truncated
	cout << i << endl;

	i = divide_integer(5,2);//no conversion
	cout << i << endl;
	i = divide_integer(5.1,2.2);//5.1 and 2.2 truncated
	cout << i << endl;
	f = divide_integer(5,2);//return truncated
	cout << f << endl;
	f = divide_integer(5.1,2.2);//5.1 and 2.2 truncated and return converted
	cout << f << endl;
}
