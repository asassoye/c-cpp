#include <iostream>

using namespace std;

void f(char tab[]) //== void f(char* tab)
{
	cout << sizeof(tab) / sizeof(*tab) << endl; // sizeof(char*) / 1
												//   |-> = 8 en x64
}

int main()
{
	char tab[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g'}; //type tab : char*

	cout << sizeof(tab) / sizeof(*tab) << endl; // (7*1) / 1

	f(tab);
}
