#include <iostream>

using namespace std;

void swap(int * x, int * y)
{
	cout << "Entering swap : " << *x << " " << *y << endl;	
	cout << "Adresses : " << x << " " << y << endl;
	cout << "Adresses of adresses : " << &x << " " << &y << endl<< endl;

	int* tmp = y;
	y = x;
	x = tmp;

	cout << "Exiting swap : " << *x << " " << *y << endl;
	cout << "Adresses : " << x << " " << y << endl;
	cout << "Adresses of adresses : " << &x << " " << &y << endl<< endl;
}

int main()
{
	int i = 1;
	int j = 2;

	int  * pti = &i;
	int  * ptj = &j;

	cout << "Before call : " << i << " " << j << endl;
	cout << "Adresses : " << &i << " " << &j << endl;
	cout << "Adresses of adresses : " << &pti << " " << &ptj << endl<< endl;
	swap(pti, ptj);
	cout << "After call : " << i << " " << j << endl;
	cout << "Adresses : " << &i << " " << &j << endl;
	cout << "Adresses of adresses : " << &i << " " << &j << endl<< endl;
}
