#include <iostream>

using namespace std;

void fi(int & i) { cout << "fi " << i << endl; }
void fd(double & f) { cout << "fd " << f << endl; }

int main()
{
	int i = 2;
	double j = 3.5;

	int & ri = i;
	int * pti = &i;
	double & rj = j;
	double * ptj = &j;
	
	ri = j;
	ri = rj;
	//pti = ptj; //ko
	rj = i;
	rj = ri;
	//ptj = pti; //ko

	//fi(rj); //ko
	//fd(ri); //ko
}
