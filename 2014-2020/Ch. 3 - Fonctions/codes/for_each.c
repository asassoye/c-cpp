#include <stdio.h>

/**
void for_each(int tab[], int n, void (*f)(int*))
{
	for(int i = 0; i < n; i++)
		f(&tab[i]);
}
*/

void for_each(void* tab, int n, size_t t, void (*f)(void*))
{
	for(int i = 0; i < n; i++)
	{
		f(&tab);

		tab = tab + t;
	}
}

void print_ptr_i(void * i)
{
	printf("%d ", *(int*)i);
}

void plus_one(void * i)
{
	*(int*)i = *(int*)i + 1;
}

int main()
{
	int tab[] = {1,2,3,4,5};

	for_each(tab, 5, sizeof(int), &print_ptr_i);
	printf("\n");

	for_each(tab, 5, sizeof(int), &plus_one);
	for_each(tab, 5, sizeof(int), &print_ptr_i);
	printf("\n");
}
