#include <iostream>

using namespace std;

int & f()
{
	int i = 2;
	return i; //not supposed to compile
}

int * g()
{
	int i = 2;
	return &i; //bad idea
}

int main()
{
	int & i = f();
	cout << i << endl;//if it compiles, then undefined behaviour

	int * pi = g();
	cout << *pi << endl; //undefined
}
