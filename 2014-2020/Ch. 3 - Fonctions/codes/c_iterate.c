#include <stdio.h>

int main()
{	
	int tab[] = {1, 2, 3, 4, 5};

	for(int i = 0; i < 5; i++)
		printf("%d ", tab[i]);
	printf("\n");

	int * it = tab;
	
	int i = 0;
	while(i != 5)
	{
		printf("%d ", *it);
		it++;
		i++;
	}
	printf("\n");

	//                it
	// 1  2  3  4  5  ?? 	
	it--;

	while(i != 0)
	{
		printf("%d ", *it);
		it--;
		i--;
	}
	printf("\n");
}
